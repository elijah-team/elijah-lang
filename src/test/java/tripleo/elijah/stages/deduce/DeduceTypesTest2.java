/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce;

import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import tripleo.elijah.comp.Compilation;
import tripleo.elijah.comp.IO;
import tripleo.elijah.comp.PipelineLogic;
import tripleo.elijah.comp.StdErrSink;
import tripleo.elijah.contexts.FunctionContext;
import tripleo.elijah.lang.ClassHeader;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.FunctionDef;
import tripleo.elijah.lang.IdentExpression;
import tripleo.elijah.lang.NormalTypeName;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.lang.OS_Type;
import tripleo.elijah.lang.Qualident;
import tripleo.elijah.lang.Scope3;
import tripleo.elijah.lang.VariableSequence;
import tripleo.elijah.lang.VariableStatement;
import tripleo.elijah.lang.VariableTypeName;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.ClassStatementBuilder;
import tripleo.elijah.lang.FunctionDef;
import tripleo.elijah.lang.IdentExpression;
import tripleo.elijah.lang.NormalTypeName;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.lang.OS_Type;
import tripleo.elijah.lang.Qualident;
import tripleo.elijah.lang.Scope3;
import tripleo.elijah.lang.VariableSequence;
import tripleo.elijah.lang.VariableStatement;
import tripleo.elijah.lang.VariableTypeName;
import tripleo.elijah.stages.gen_fn.GenType;
import tripleo.elijah.stages.gen_fn.GeneratePhase;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah.util.Helpers;

import static tripleo.elijah.util.Helpers.List_of;

public class DeduceTypesTest2 {

	@Test
	public void testDeduceIdentExpression() throws ResolveError {
		final Compilation c = new Compilation(new StdErrSink(), new IO());

		//final ClassStatement cs = new ClassStatement(mod, mod.getContext());

		final OS_Module mod = c.moduleBuilder()
				.withPrelude("c")
				.setContext()
				.build();
		final ClassStatement cs = new ClassStatementBuilder()
				.setParentElement(mod)
				.setParentContext(mod.getContext())
				.setName(Helpers.string_to_ident("Test"))
				.createClassStatement();

		final ClassHeader ch = new ClassHeader(false, List_of());
		ch.setName(Helpers.string_to_ident("Test"));
		cs.setHeader(ch);

		final FunctionDef fd = cs.funcDef();
		fd.setName((Helpers.string_to_ident("test")));
		final Scope3 scope3 = new Scope3(fd);
		final VariableSequence vss = scope3.varSeq();
		final VariableStatement vs = vss.next();
		vs.setName((Helpers.string_to_ident("x")));
		final Qualident qu = new Qualident();
		qu.append(Helpers.string_to_ident("SystemInteger"));
		((NormalTypeName)vs.typeName()).setName(qu);
		final FunctionContext fc = (FunctionContext) fd.getContext();
		vs.typeName().setContext(fc);
		final IdentExpression x1 = Helpers.string_to_ident("x");
		x1.setContext(fc);
		fd.scope(scope3);
		fd.postConstruct();
		cs.postConstruct();
		mod.postConstruct();

		//
		//
		//
		final ElLog.Verbosity verbosity1 = c.gitlabCIVerbosity();

		final PipelineLogic pl = new PipelineLogic(verbosity1);
		final GeneratePhase generatePhase = pl.generatePhase;
			//new GeneratePhase(verbosity1, pl);
		final DeducePhase dp = pl.dp;
			//new DeducePhase(generatePhase, pl, verbosity1);
		assert generatePhase != null;
		assert dp != null;

		final DeduceTypes2 d = dp.deduceModule(mod, dp.generatedClasses, verbosity1);

		final GenType x = DeduceLookupUtils.deduceExpression(d, x1, fc);
		System.out.println("GenType: " + x.asString());

//		Assert.assertEquals(new OS_Type(BuiltInTypes.SystemInteger).getBType(), x.getBType());
//		final RegularTypeName tn = new RegularTypeName();
		final VariableTypeName tn = new VariableTypeName();
		final Qualident tnq = new Qualident();
		tnq.append(Helpers.string_to_ident("SystemInteger"));
		tn.setName(tnq);

		//
		//
		assert fd.getContext() == fc;
		//
		//

		tn.setContext(fd.getContext());

//		Assert.assertEquals(new OS_Type(tn).getTypeName(), x.getTypeName());
		final GenType resolvedType = d.resolve_type(new OS_Type(tn), tn.getContext());
		Assert.assertTrue(genTypeEquals(resolvedType, x));
//		Assert.assertEquals(new OS_Type(tn).toString(), x.toString());
	}

	private boolean genTypeEquals(@NotNull GenType a, @NotNull GenType b) {
		// TODO hack
		return a.typeName.equals(b.typeName) &&
				a.resolved.equals(b.resolved);
	}
}
