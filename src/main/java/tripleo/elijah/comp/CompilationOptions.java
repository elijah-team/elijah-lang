package tripleo.elijah.comp;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.diagnostic.Diagnostic;
import tripleo.elijah.diagnostic.Locatable;

import java.io.File;
import java.io.PrintStream;
import java.util.List;
import java.util.regex.Pattern;

import static tripleo.elijah.util.Helpers.List_of;

final class CompilationOptions {
	private final Compilation        c;
	private       boolean            do_out;
	Compilation.Stages stage = CompilationAlways.defaultStage();
	boolean            showTree;

	public CompilationOptions(final Compilation aCompilation) {
		c = aCompilation;
	}

	public String @Nullable [] action(final @NotNull List<String> args) throws ParseException {
		if (args.size() <= 0) {
			usage();
			return null;
		}

		final Options           options = getOptions();
		final CommandLineParser clp     = new DefaultParser();
		final CommandLine       cmd     = clp.parse(options, args.toArray(new String[args.size()]));

		if (cmd.hasOption("s")) {
			final String cov = cmd.getOptionValue('s');
			stage = Compilation.Stages.valueOf(cov);
		}
		if (cmd.hasOption("showtree")) {
			showTree = true;
		}
		if (cmd.hasOption("out")) {
			do_out = true;
		}
		if (Compilation.isGitlab_ci() || cmd.hasOption("silent")) {
		}

		return cmd.getArgs();
	}

	private void usage() {
		CompilationAlways.usage();
	}

	@NotNull
	private Options getOptions() {
		return CompilationAlways.getOptions();
	}

	public void action2(final @Nullable String[] args2, final ErrSink errSink) throws Exception {
		if (args2 == null)
			return;

		for (final String file_name : args2) {
			action2_action(file_name, errSink);
		}
	}

	private void action2_action(final String file_name, final ErrSink errSink) throws Exception {
		final File    f        = new File(file_name);
		final boolean matches2 = Pattern.matches(".+\\.ez$", file_name);
		if (matches2)
			add_ci(parseEzFile(f, file_name, errSink));
		else {
			if (f.isDirectory()) {
				final List<CompilerInstructions> ezs = searchEzFiles(f);
				if (ezs.size() > 1) {
					errSink.reportDiagnostic(new TooManyEzFiles());
					//errSink.reportError("9997 Too many .ez files, be specific.");
				} else if (ezs.size() == 0) {
					errSink.reportDiagnostic(new NoEzFiles());
					//errSink.reportError("9999 No .ez files found.");
				} else {
					CompilerInstructions ez_file = ezs.get(0);
					add_ci(ez_file);
				}
			} else
				errSink.reportError("9995 Not a directory " + f.getAbsolutePath());
		}
	}

	static class TooManyEzFiles implements Diagnostic {
		@Override
		public String code() {
			return "9997";
		}

		@Override
		public Severity severity() {
			return Severity.ERROR;
		}

		@Override
		public @NotNull Locatable primary() {
			return null; // TODO Interesting dilemma
		}

		@Override
		public @NotNull List<Locatable> secondary() {
			return null;
		}

		@Override
		public void report(@NotNull PrintStream stream) {
			stream.println(String.format("---[%s]---: %s", code(), message()));
			// linecache.print(primary);
			for (Locatable sec : secondary()) {
				//linecache.print(sec)
			}
			stream.flush();
		}

		private @NotNull String message() {
			return "Too many .ez files, be specific.";
		}
	}

	static class NoEzFiles implements Diagnostic {

		@Override
		public String code() {
			return "9999";
		}

		@Override
		public Severity severity() {
			return Severity.ERROR;
		}

		@Override
		public @NotNull Locatable primary() {
			return null; // TODO Another interesting dilemma
		}

		@Override
		public @NotNull List<Locatable> secondary() {
			return List_of();
		}

		@Override
		public void report(@NotNull PrintStream stream) {
			stream.println(String.format("---[%s]---: %s", code(), message()));
			// linecache.print(primary);
			for (Locatable sec : secondary()) {
				//linecache.print(sec)
			}
			stream.flush();
		}

		private @NotNull String message() {
			return "No .ez files found.";
		}
	}

	private CompilerInstructions parseEzFile(final File aFile, final String aFile_name, final ErrSink aErrSink) throws Exception {
		return c.parseEzFile(aFile, aFile_name, aErrSink);
	}

	private @NotNull List<CompilerInstructions> searchEzFiles(final File aF) {
		return c.searchEzFiles(aF);
	}

	private void add_ci(final @NotNull CompilerInstructions aCompilerInstructions) {
		c.add_ci(aCompilerInstructions);
	}

	private void use(final @NotNull CompilerInstructions ci, final boolean do_out) throws Exception {
		c.use(ci, do_out);
	}

	private void findStdLib(final String s) {
		c.findStdLib(s);
	}

	public void action3(final @NotNull List<CompilerInstructions> cis) throws Exception {
		final @NotNull CompilerInstructions rootCI = cis.get(0);
		System.err.println("130 GEN_LANG: " + rootCI.genLang());

		c.rootCI = rootCI; // TODO ...

		findStdLib("c"); // TODO find a better place for this

		for (final CompilerInstructions ci : cis) {
			use(ci, do_out);
		}

		final ICompilationAccess ca = new DefaultCompilationAccess(c);
		final ProcessRecord      pr = new ProcessRecord(ca);
		final RuntimeProcess	 rt = StageToRuntime.get(stage, ca, pr);

		rt.prepare();
		rt.run(c);
		rt.postProcess(pr, ca);
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
