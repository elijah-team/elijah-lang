/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp;

import org.jetbrains.annotations.NotNull;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.gen_c.GenerateC;
import tripleo.elijah.stages.gen_fn.GeneratedNode;
import tripleo.elijah.stages.gen_generic.GenerateResult;
import tripleo.elijah.stages.gen_generic.GenerateResultItem;
import tripleo.elijah.work.WorkManager;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created 8/21/21 10:16 PM
 */
public class GeneratePipeline implements PipelineMember {
	private final Compilation    c;
	private final DeducePipeline dpl;

	public GeneratePipeline(Compilation aCompilation, DeducePipeline aDpl) {
		c = aCompilation;
		dpl = aDpl;
	}

	@Override
	public void run() {
		final PipelineLogic       pl  = c.pipelineLogic;
		final List<GeneratedNode> lgc = dpl.lgc;

		if (lgc.size() == 0)
			return; // short circuit

		final WorkManager wm = new WorkManager();

		final FI fi = new FI(wm, pl, lgc);

		final List<List<GenerateResultItem>> res = pl.mods
				.stream()
				.map(mod -> f(mod, fi))
				.collect(Collectors.toList());

		final List<List<GenerateResultItem>> yy = new ArrayList<List<GenerateResultItem>>();

		for (OS_Module mod : pl.mods) {
			final List<GenerateResultItem> y = f(mod, fi);

			yy.add(y);
		}

		final List<GenerateResultItem> pgr = pl.gr.results();

		for (List<GenerateResultItem> gril : yy) {
			pgr.addAll(gril);
		}

		int y=2;
	}

	final static class FI {
		private final WorkManager         wm;
		private final PipelineLogic       pl;
		private final List<GeneratedNode> lgc;

		public FI(final WorkManager aWm, final PipelineLogic aPl, final List<GeneratedNode> aLgc) {
			wm  = aWm;
			pl  = aPl;
			lgc = aLgc;
		}

		public WorkManager getWm() {
			return wm;
		}

		public PipelineLogic getPl() {
			return pl;
		}

		public List<GeneratedNode> getLgc() {
			return lgc;
		}
	}

	private static List<GenerateResultItem> f(final @NotNull OS_Module mod, final @NotNull FI fi) {
		final ErrSink        errSink   = mod.getCompilation().getErrSink();

		final GenerateC generateC = new GenerateC(mod, errSink, fi.pl.getVerbosity(), fi.pl);

		final GenerateResult ggr = fi.pl.run3(mod, fi.lgc, fi.wm, generateC);

		fi.wm.drain();

		return ggr.results();
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
