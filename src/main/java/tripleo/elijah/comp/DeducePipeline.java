/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp;

import tripleo.elijah.lang.AbstractCodeGen;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.NamespaceStatement;
import tripleo.elijah.lang.OS_Element2;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.gen_fn.GeneratedNode;
import tripleo.elijah.stages.translate.TranslateModule;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * Created 8/21/21 10:10 PM
 */
public class DeducePipeline implements PipelineMember {
	private final @NotNull Compilation c;
	private final List<PipelineLogicRunnable> plrs = new ArrayList<PipelineLogicRunnable>();
	List<GeneratedNode> lgc = new ArrayList<GeneratedNode>();

	public DeducePipeline(@NotNull Compilation aCompilation) {
		System.err.println("***** Hit DeducePipeline constructor");

		c = aCompilation;

		for (final OS_Module module : c.modules) {
			if (false) {
				addRunnable(new PL_JavaBull(module));
			} else {
				addRunnable(new PL_AddModule(module));
			}
		}

		addRunnable(new PL_EverythingBeforeGenerate());
		addRunnable(new PL_SaveGeneratedClasses());
	}

	@Override
	public void run() {
		// TODO move "futures" to ctor...
		//c.pipelineLogic.everythingBeforeGenerate(lgc);
		//lgc = c.pipelineLogic.dp.generatedClasses.copy();

		// TODO wait for these two to finish...
		// TODO make sure you call #setPipelineLogic...

		assert c.pipelineLogic != null;

		// TODO jul-03: why not just inline setPipelineLogic ??
		setPipelineLogic(c.pipelineLogic);
	}

	public void setPipelineLogic(final PipelineLogic aPipelineLogic) {
		for (PipelineLogicRunnable plr : plrs) {
			plr.run(aPipelineLogic);
		}
	}

	private void addRunnable(final PipelineLogicRunnable plr) {
		plrs.add(plr);
	}

	private interface PipelineLogicRunnable {
		void run(final PipelineLogic pipelineLogic);
	}

	private static class PL_JavaBull implements PipelineLogicRunnable {
		private final OS_Module m;

		public PL_JavaBull(final OS_Module aModule) {
			m = aModule;
		}

		static final class DeduceTypes {
			public DeduceTypes(final OS_Module aModule) {
			}

			public void deduce() {
			}
		}

		static final class ExpandFunctions {
			public ExpandFunctions(final OS_Module aModule) {
			}

			public void expand() {
			}
		}

		static final class JavaCodeGen extends AbstractCodeGen {
		}

		@Override
		public void run(final @NotNull PipelineLogic pipelineLogic) {
			new DeduceTypes(m).deduce(); // !! DeduceTypes. this is that old??

			for (final OS_Element2 item : m.items()) {
				if (item instanceof ClassStatement || item instanceof NamespaceStatement) {
					System.err.println("8001 " + item);
				}
			}

			new TranslateModule(m).translate();
			new ExpandFunctions(m).expand();

			final JavaCodeGen visit = new JavaCodeGen();
			m.visitGen(visit);
		}
	}

	private static class PL_AddModule implements PipelineLogicRunnable {
		private final OS_Module m;

		public PL_AddModule(final OS_Module aModule) {
			m = aModule;
		}

		@Override
		public void run(final @NotNull PipelineLogic pipelineLogic) {
			pipelineLogic.addModule(m);
		}
	}

	private class PL_EverythingBeforeGenerate implements PipelineLogicRunnable {
		@Override
		public void run(final @NotNull PipelineLogic pipelineLogic) {
			try {
				pipelineLogic.everythingBeforeGenerate(lgc);
			} catch (Throwable aE) {
				throw new RuntimeException(aE);
			}
		}
	}

	private class PL_SaveGeneratedClasses implements PipelineLogicRunnable {
		@Override
		public void run(final @NotNull PipelineLogic pipelineLogic) {
			lgc = pipelineLogic.dp.generatedClasses.copy();
		}
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
