package tripleo.elijah.comp;

import org.apache.commons.cli.Options;
import org.jetbrains.annotations.NotNull;

class CompilationAlways {
	public static void usage() {
		System.err.println("Usage: eljc [--showtree] [-sE|O] <directory or .ez file names>");
	}

	public static @NotNull Options getOptions() {
		final Options options = new Options();
		options.addOption("s", true, "stage: E: parse; O: output");
		options.addOption("showtree", false, "show tree");
		options.addOption("out", false, "make debug files");
		options.addOption("silent", false, "suppress DeduceType output to console");
		return options;
	}

	public static Compilation.Stages defaultStage() {
		return Compilation.Stages.D; // TODO default??
	}
}

//
//
//
