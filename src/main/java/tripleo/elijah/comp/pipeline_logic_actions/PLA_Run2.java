/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp.pipeline_logic_actions;

import io.reactivex.rxjava3.functions.Consumer;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.PipelineLogic;
import tripleo.elijah.comp.PipelineLogicAction;
import tripleo.elijah.entrypoints.EntryPoint;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.deduce.DeducePhase;
import tripleo.elijah.stages.gen_fn.*;

import java.util.List;

public class PLA_Run2 implements PL_Action {
	private final OS_Module                      mod;

	final         Consumer<GeneratedFunction>    fa = new Consumer<GeneratedFunction>() {
		@Override
		public void accept(final @NotNull GeneratedFunction aGeneratedFunction) throws Throwable {
			if (aGeneratedFunction.getCode() == 0)
				aGeneratedFunction.setCode(next());
		}

		private int next() {
			return mod.getCompilation().nextFunctionCode();
		}
	};
	final         Consumer<GeneratedContainerNC> ca = new Consumer<GeneratedContainerNC>() {
		@Override
		public void accept(final @NotNull GeneratedContainerNC aGeneratedContainerNC) throws Throwable {
			if (aGeneratedContainerNC.getCode() == 0)
				aGeneratedContainerNC.setCode(next());
		}

		private int next() {
			return mod.getCompilation().nextClassCode();
		}
	};

	public PLA_Run2(final OS_Module aMod) {
		mod = aMod;
	}

	@Override
	public void run(final @NotNull PipelineLogic pl) throws Throwable {
		final @NotNull DeducePhase.GeneratedClasses lgc1 = pl.dp.generatedClasses;
		final @NotNull List<EntryPoint>             epl  = mod.entryPoints;
		final PipelineLogicAction                   pla  = new PipelineLogicAction(pl, mod, lgc1);

		pla.act(epl);
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
