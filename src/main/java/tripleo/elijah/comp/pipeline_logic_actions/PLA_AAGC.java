/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp.pipeline_logic_actions;

import tripleo.elijah.comp.PipelineLogic;
import tripleo.elijah.stages.deduce.DeducePhase;
import tripleo.elijah.stages.gen_fn.GeneratedNode;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class PLA_AAGC implements PL_Action {
	private final DeducePhase.GeneratedClasses generatedClasses;
	private final List<GeneratedNode>          lgc;

	@Contract(pure = true)
	public PLA_AAGC(final DeducePhase.GeneratedClasses aGeneratedClasses, final List<GeneratedNode> aLgc) {
		generatedClasses = aGeneratedClasses;
		lgc              = aLgc;
	}

	@Contract(pure = true)
	public PLA_AAGC(final @NotNull EBG_State st) {
		this(st.dp().generatedClasses, st.lgc);
	}

	@Override
	public void run(final PipelineLogic pl) throws Throwable {
		generatedClasses.addAll(lgc);
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
