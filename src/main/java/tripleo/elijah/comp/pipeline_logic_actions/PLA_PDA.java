/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp.pipeline_logic_actions;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.comp.PipelineLogic;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.deduce.DeducePhase;
import tripleo.elijah.stages.post_deduce.PostDeduce;

import java.util.Collection;

public class PLA_PDA implements PL_Action {
	private final Collection<OS_Module> mods;
	private final DeducePhase           dp;

	public PLA_PDA(final Collection<OS_Module> aMods, final DeducePhase aDp) {
		mods = aMods;
		dp   = aDp;
	}

	@Contract(pure = true)
	public PLA_PDA(final @NotNull EBG_State st) {
		this(st.mods(), st.dp());
	}

	@Override
	public void run(final @NotNull PipelineLogic pl) throws Throwable {
		if (pl.postDeduceEnabled) {
			for (OS_Module mod : mods) {
				PostDeduce pd = new PostDeduce(mod.getCompilation().getErrSink(), dp);
				pd.analyze();
			}
		}
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
