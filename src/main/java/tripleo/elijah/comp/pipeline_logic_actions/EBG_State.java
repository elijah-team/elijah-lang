/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp.pipeline_logic_actions;

import tripleo.elijah.comp.PipelineLogic;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.deduce.DeducePhase;
import tripleo.elijah.stages.gen_fn.GeneratedNode;

import java.util.List;

/**
 * Created 12/30/20 2:14 AM
 */
public class EBG_State {
	// private final PipelineLogic       pipelineLogic;
	private final DeducePhase         dp;
	private final List<OS_Module>     mods;
	final         List<GeneratedNode> lgc;

	public EBG_State(final PipelineLogic ignoredAPipelineLogic,
					 final DeducePhase aDp,
					 final List<OS_Module> aMods,
					 final List<GeneratedNode> aLgc) {

		// pipelineLogic = aPipelineLogic;
		dp            = aDp;
		mods          = aMods;
		lgc           = aLgc;
	}

	public DeducePhase dp() {
		return dp;
	}

	public List<OS_Module> mods() {
		return mods;
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
