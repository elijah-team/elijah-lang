/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp;

import antlr.RecognitionException;
import antlr.TokenStreamException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.ci.CompilerInstructions;
import tripleo.elijah.ci.LibraryStatementPart;
import tripleo.elijah.comp.queries.QueryEzFileToModule;
import tripleo.elijah.comp.queries.QueryEzFileToModuleParams;
import tripleo.elijah.comp.queries.QuerySourceFileToModule;
import tripleo.elijah.comp.queries.QuerySourceFileToModuleParams;
import tripleo.elijah.contexts.ModuleContext;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.lang.OS_Package;
import tripleo.elijah.lang.Qualident;
import tripleo.elijah.nextgen.query.Operation;
import tripleo.elijah.stages.deduce.FunctionMapHook;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah.util.Helpers;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.regex.Pattern;

import static tripleo.elijah.nextgen.query.Mode.SUCCESS;

public class Compilation {

	public String getProjectName() {
		return rootCI.getName();
	}

	enum Stages {
		E("E"),
		D("D"),
		S("S"),  // ??
		O("O")  // Output
		;

		final String s;

		Stages(final String aO) {
			s = aO;
		}
	}

	public final  List<OS_Module>            modules   = new ArrayList<OS_Module>();
	public final  List<CompilerInstructions> cis       = new ArrayList<CompilerInstructions>();
	private final int                        _compilationNumber;
	private final ErrSink                    eee;
	private final Pipeline                   pipelines = new Pipeline();

	public PipelineLogic pipelineLogic;
	public Stages        stage = Stages.O;
	CompilerInstructions rootCI;
	boolean              showTree = false;

	private IO  io;
	private int _classCode    = 101;
	private int _functionCode = 1001;

	public Compilation(final ErrSink aErrSink, final IO aIO) {
		eee                = aErrSink;
		io                 = aIO;
		_compilationNumber = new Random().nextInt(Integer.MAX_VALUE);
		cez                = new CEZ(eee, this);
		use                = new USE(this);
	}

	public static ElLog.Verbosity gitlabCIVerbosity() {
		final boolean gitlab_ci = isGitlab_ci();
		return gitlab_ci ? ElLog.Verbosity.SILENT : ElLog.Verbosity.VERBOSE;
	}

	public static boolean isGitlab_ci() {
		return System.getenv("GITLAB_CI") != null;
	}

	public void feedCmdLine(final List<String> args) {
		try {
			final ErrSink errSink = eee == null ? new StdErrSink() : eee;

			main(args, errSink);
		} catch (Exception aE) {
			throw new RuntimeException(aE);
		}
	}

	public IO getIO() {
		return io;
	}

	public void setIO(final IO io) {
		this.io = io;
	}

	public void main(final @NotNull List<String> args, final @NotNull ErrSink errSink) throws Exception {
		final CompilationOptions opt   = new CompilationOptions(this);
		final String[]           args2 = opt.action(args);

		stage    = opt.stage;
		showTree = opt.showTree;

		opt.action2(args2, errSink);

		//if (cis.size() > 0)
		opt.action3(cis);
	}

	public void addPipeline(final PipelineMember aPipelineMember) {
		pipelines.add(aPipelineMember);
	}

	public Pipeline getPipelines() {
		return pipelines;
	}

	static class CEZ {
		private final FilenameFilter filter = new FilenameFilter() {
			@Override
			public boolean accept(final File file, final String s) {
				final boolean matches2 = Pattern.matches(".+\\.ez$", s);
				return matches2;
			}
		};

		private final ErrSink     errSink;
		private final Compilation c;

		private final Map<String, CompilerInstructions> fn2ci = new HashMap<String, CompilerInstructions>();

		CEZ(final ErrSink aErrSink, final Compilation aC) {
			errSink = aErrSink;
			c       = aC;
		}

		List<CompilerInstructions> searchEzFiles(final @NotNull File directory) {
			final List<CompilerInstructions> R    = new ArrayList<CompilerInstructions>();
			final String[]                   list = directory.list(filter);

			if (list != null) {
				for (final String file_name : list) {
					try {
						final File                 file   = new File(directory, file_name);
						final CompilerInstructions ezFile = parseEzFile(file, file.toString(), errSink);

						if (ezFile != null)
							R.add(ezFile);
						else
							errSink.reportError("9995 ezFile is null " + file);
					} catch (final Exception e) {
						errSink.exception(e);
					}
				}
			}
			return R;
		}

		private @Nullable CompilerInstructions parseEzFile(final @NotNull File aFile, final String aS, final ErrSink aErrSink) throws Exception {
			final String absolutePath = aFile.getAbsolutePath();

			System.out.printf("   %s%n", absolutePath);
			if (!aFile.exists()) {
				aErrSink.reportError("File doesn't exist " + absolutePath);
				return null;
			}

			final CompilerInstructions m = c.realParseEzFile(aS, c.io.readFile(aFile), aFile);
			{
				final String gl = m.genLang();
				final String prelude;

				System.err.println("230 " + gl);

				// TODO should be java for eljc
				prelude = gl == null ? "c" : gl;
			}
			return m;
		}

		public CompilerInstructions realParseEzFile(final String f, final InputStream s, final File file) throws IOException {
			final String absolutePath = file.getCanonicalFile().toString();
			if (fn2ci.containsKey(absolutePath)) { // don't parse twice
				return fn2ci.get(absolutePath);
			}

			final Operation<CompilerInstructions> cio = parseEzFile_(f, s);
			if (cio.mode() != SUCCESS) {
				final Exception e = cio.failure();
				assert e != null;

				System.err.printf("parser exception: %s%n", e);
				e.printStackTrace(System.err);
				s.close();
				return null;
			}
			final CompilerInstructions R = cio.success();
			R.setFilename(file.toString());
			fn2ci.put(absolutePath, R);
			s.close();
			return R;
		}

		private Operation<CompilerInstructions> parseEzFile_(final String f, final InputStream s) {
			final QueryEzFileToModuleParams qp = new QueryEzFileToModuleParams(f, s);
			return new QueryEzFileToModule(qp).calculate();
		}
	}

	private final CEZ cez;

	List<CompilerInstructions> searchEzFiles(final @NotNull File directory) {
		return cez.searchEzFiles(directory);
	}

	void add_ci(final CompilerInstructions ci) {
		cis.add(ci);
	}

	public void use(final @NotNull CompilerInstructions compilerInstructions, final boolean do_out) throws Exception {
		final File instruction_dir = new File(compilerInstructions.getFilename()).getParentFile();
		for (final LibraryStatementPart lsp : compilerInstructions.lsps) {
			final String dir_name = Helpers.remove_single_quotes_from_string(lsp.getDirName());
			final File   dir;

			if (dir_name.equals(".."))
				dir = instruction_dir/*.getAbsoluteFile()*/.getParentFile();
			else
				dir = new File(instruction_dir, dir_name);

			use_internal(dir, do_out, lsp);
		}

		final LibraryStatementPart lsp = new LibraryStatementPart();
		lsp.setName(Helpers.makeToken("default")); // TODO: make sure this doesn't conflict
		lsp.setDirName(Helpers.makeToken(String.format("\"%s\"", instruction_dir)));
		lsp.setInstructions(compilerInstructions);
		use_internal(instruction_dir, do_out, lsp);
	}

	static class USE {
		private final FilenameFilter accept_source_files = new FilenameFilter() {
			@Override
			public boolean accept(final File directory, final String file_name) {
				final boolean matches = Pattern.matches(".+\\.elijah$", file_name)
						|| Pattern.matches(".+\\.elijjah$", file_name);
				return matches;
			}
		};

		private final Compilation c;
		private final ErrSink     eee;

		USE(final Compilation aC) {
			c = aC;
			//
			eee = c.eee;
		}

		private void use_internal(final @NotNull File dir, final boolean do_out, LibraryStatementPart lsp) throws Exception {
			if (!dir.isDirectory()) {
				eee.reportError("9997 Not a directory " + dir);
				return;
			}

			final File[] files = dir.listFiles(accept_source_files);
			if (files != null) {
				for (final File file : files) {
					parseElijjahFile(file, file.toString(), eee, do_out, lsp);
				}
			}
		}

		private void parseElijjahFile(@NotNull final File f,
									  final String file_name,
									  final ErrSink errSink,
									  final boolean do_out,
									  LibraryStatementPart lsp) throws Exception {
			final String absolutePath = f.getAbsolutePath();
			System.out.printf("   %s%n", absolutePath);

			if (f.exists()) {
				final OS_Module m = realParseElijjahFile(file_name, f, do_out);
				m.setLsp(lsp);
				m.prelude = findPrelude("c"); // TODO we dont know which prelude to find yet
			} else {
				errSink.reportError("File doesn't exist " + absolutePath); // TODO getPath?? and use a Diagnostic
			}
		}

		public OS_Module realParseElijjahFile(final String f, final File file, final boolean do_out) throws Exception {
			return c.realParseElijjahFile(f, file, do_out);
		}

		public OS_Module findPrelude(final String prelude_name) {
			return c.findPrelude(prelude_name);
		}
	}

	private final USE use;

	private void use_internal(final @NotNull File dir, final boolean do_out, LibraryStatementPart lsp) throws Exception {
		use.use_internal(dir, do_out, lsp);
	}

	CompilerInstructions parseEzFile(final File f, final String file_name, final ErrSink errSink) throws Exception {
		return cez.parseEzFile(f, file_name, errSink);
	}

	public OS_Module realParseElijjahFile(final String f, final File file, final boolean do_out) throws Exception {
		return cej.realParseElijjahFile(f, file, do_out);
	}

	private final CEJ cej = new CEJ(this);

	static class CEJ {
		private final Map<String, OS_Module> fn2m = new HashMap<String, OS_Module>();
		private final Compilation            c;

		public CEJ(final Compilation aCompilation) {
			c = aCompilation;
		}

		public OS_Module realParseElijjahFile(final String f, final @NotNull File file, final boolean do_out) throws Exception {
			final String absolutePath = file.getCanonicalFile().toString();
			if (fn2m.containsKey(absolutePath)) { // don't parse twice
				return fn2m.get(absolutePath);
			}
			final InputStream s = c.io.readFile(file);
			//try {
			final Operation<OS_Module> om = parseFile_(f, s, do_out);
			if (om.mode() != SUCCESS) {
				final Exception e = om.failure();
				assert e != null;

				System.err.println(("parser exception: " + e));
				e.printStackTrace(System.err);
				s.close();
				return null;
			}
			final OS_Module R = om.success();
			fn2m.put(absolutePath, R);
			s.close();
			return R;
		}

		private Operation<OS_Module> parseFile_(final String f, final InputStream s, final boolean do_out) {
			final QuerySourceFileToModuleParams qp = new QuerySourceFileToModuleParams(s, f, do_out);
			return new QuerySourceFileToModule(qp, c).calculate();
		}

		public void addModule(final OS_Module module, final String fn) {
			c.modules.add(module);
			fn2m.put(fn, module);
		}
	}

	public CompilerInstructions realParseEzFile(final String f, final InputStream s, final File file) throws Exception {
		return cez.realParseEzFile(f, s, file);
	}

	public static class ModuleBuilder {
//		private final Compilation compilation;
		private final OS_Module mod;
		private       boolean   _addToCompilation = false;
		private       String    _fn               = null;

		public ModuleBuilder(Compilation aCompilation) {
//			compilation = aCompilation;
			mod = new OS_Module();
			mod.setCompilation(aCompilation);
		}

		public ModuleBuilder setContext() {
			final ModuleContext mctx = new ModuleContext(mod);
			mod.setContext(mctx);
			return this;
		}

		public OS_Module build() {
			if (_addToCompilation) {
				if (_fn == null) throw new IllegalStateException("Filename not set in ModuleBuilder");
				mod.getCompilation().addModule(mod, _fn);
			}
			return mod;
		}

		public ModuleBuilder withPrelude(String aPrelude) {
			mod.prelude = mod.getCompilation().findPrelude("c");
			return this;
		}

		public ModuleBuilder withFileName(String aFn) {
			_fn = aFn;
			mod.setFileName(aFn);
			return this;
		}

		public ModuleBuilder addToCompilation() {
			_addToCompilation = true;
			return this;
		}
	}

	public ModuleBuilder moduleBuilder() {
		return new ModuleBuilder(this);
	}

	private Operation<CompilerInstructions> parseEzFile_(final String f, final InputStream s) throws RecognitionException, TokenStreamException {
		final QueryEzFileToModuleParams qp = new QueryEzFileToModuleParams(f, s);
		return new QueryEzFileToModule(qp).calculate();
	}

	public List<ClassStatement> findClass(final String aClassName) {
		final List<ClassStatement> l = new ArrayList<ClassStatement>();
		for (final OS_Module module : modules) {
			if (module.hasClass(aClassName)) {
				l.add((ClassStatement) module.findClass(aClassName));
			}
		}
		return l;
	}

	public int errorCount() {
		return eee.errorCount();
	}

	public OS_Module findPrelude(final String prelude_name) {
		final File local_prelude = new File("lib_elijjah/lib-" + prelude_name + "/Prelude.elijjah");
		if (local_prelude.exists()) {
			try {
				return realParseElijjahFile(local_prelude.getName(), local_prelude, false);
			} catch (final Exception e) {
				eee.exception(e);
				return null;
			}
		}
		return null;
	}

	public boolean findStdLib(final String prelude_name) {
		boolean    result       = false;
		final File local_stdlib = new File("lib_elijjah/lib-" + prelude_name + "/stdlib.ez");

		if (local_stdlib.exists()) {
			try {
				final String               stdlibName   = local_stdlib.getName();
				final InputStream          stdlibStream = io.readFile(local_stdlib);
				final CompilerInstructions ci           = realParseEzFile(stdlibName, stdlibStream, local_stdlib);

				add_ci(ci);
				result = true;
			} catch (final Exception e) {
				eee.exception(e);
			}
		}

		return result;
	}

	//
	// region MODULE STUFF
	//

	public void addModule(final OS_Module module, final String fn) {
		cej.addModule(module, fn);
	}

	// endregion

	//
	// region CLASS AND FUNCTION CODES
	//

	public int nextClassCode() {
		return _classCode++;
	}

	public int nextFunctionCode() {
		return _functionCode++;
	}

	// endregion

	//
	// region PACKAGES
	//

	public boolean isPackage(final String pkg) {
		return pm.isPackage(pkg);
	}

	@NotNull
	public OS_Package getPackage(final Qualident pkg_name) {
		return pm.getPackage(pkg_name);
	}

	@NotNull
	public OS_Package makePackage(final @NotNull Qualident pkg_name) {
		return pm.makePackage(pkg_name);
	}

	private PackageManager pm = new PackageManager(this);

	private static class PackageManager {
		private final Compilation             c;
		private final Map<String, OS_Package> _packages    = new HashMap<String, OS_Package>();
		private       int                     _packageCode = 1;

		public PackageManager(final Compilation aCompilation) {
			c = aCompilation;
		}

		@NotNull
		public OS_Package makePackage(final @NotNull Qualident pkg_name) {
			final String     packageName = pkg_name.toString();
			final OS_Package Result;

			if (!isPackage(packageName)) {
				final int        nextPackageCode = nextPackageCode();
				final OS_Package newPackage      = new OS_Package(pkg_name, nextPackageCode);

				_packages.put(packageName, newPackage);

				Result = newPackage;
			} else {
				Result = _packages.get(packageName);
				//PostconditionAnnotation
				assert Result != null;
			}

			return Result;
		}

		public boolean isPackage(final String pkg) {
			return _packages.containsKey(pkg);
		}

		public @NotNull OS_Package getPackage(final @NotNull Qualident pkg_name) {
			return _packages.get(pkg_name.toString());
		}

		public @NotNull OS_Package makePackage__(final @NotNull Qualident pkg_name) {
			final @NotNull OS_Package result;
			final String              pkg_name_str = pkg_name.toString();

			if (!isPackage(pkg_name_str)) {
				final OS_Package newPackage = new OS_Package(pkg_name, nextPackageCode());
				_packages.put(pkg_name_str, newPackage);
				result = newPackage;
			} else {
				result = _packages.get(pkg_name_str);
			}
			return result;
		}

		private int nextPackageCode() {
			return _packageCode++;
		}
	}

	// endregion

	public int compilationNumber() {
		return _compilationNumber;
	}

	public String getCompilationNumberString() {
		return String.format("%08x", _compilationNumber);
	}

	public ErrSink getErrSink() {
		return eee;
	}

	public void addFunctionMapHook(FunctionMapHook aFunctionMapHook) {
		pipelineLogic.dp.addFunctionMapHook(aFunctionMapHook);
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
