/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import tripleo.elijah.stages.gen_fn.GeneratedClass;
import tripleo.elijah.stages.gen_fn.GeneratedContainerNC;
import tripleo.elijah.stages.gen_fn.GeneratedFunction;
import tripleo.elijah.stages.gen_fn.GeneratedNamespace;

public interface CAM {
	GeneratedContainerNC one();

	Iterable<GeneratedClass> two();

	Iterable<GeneratedFunction> three();

	final class CAM_NS implements CAM {
		private final GeneratedNamespace ns;

		@Contract(pure = true)
		public CAM_NS(final GeneratedNamespace aGeneratedNamespace) {
			ns = aGeneratedNamespace;
		}

		@Contract(pure = true)
		@Override
		public GeneratedContainerNC one() {
			return ns;
		}

		@Contract(pure = true)
		@Override
		public @NotNull Iterable<GeneratedClass> two() {
			return ns.classMap.values();
		}

		@Contract(pure = true)
		@Override
		public @NotNull Iterable<GeneratedFunction> three() {
			return ns.functionMap.values();
		}
	}

	final class CAM_Factory {
		//static @NotNull CAM get(final @NotNull GeneratedNode node) {
		//	if (node instanceof GeneratedNamespace) {
		//		return new CAM.CAM_A((GeneratedNamespace) node);
		//	}
		//
		//	throw new IllegalStateException("Cannot convert node " + node);
		//	//return null;
		//}

		public static @NotNull CAM get(final @NotNull GeneratedNamespace node) {
			return new CAM_NS(node);
		}
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
