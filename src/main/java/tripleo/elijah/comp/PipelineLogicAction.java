/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp;

import io.reactivex.rxjava3.functions.Consumer;
import org.jdeferred2.DoneCallback;
import tripleo.elijah.entrypoints.EntryPoint;
import tripleo.elijah.lang.NamespaceStatement;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.deduce.DeducePhase;
import tripleo.elijah.stages.gen_fn.BaseTableEntry;
import tripleo.elijah.stages.gen_fn.GNCoded;
import tripleo.elijah.stages.gen_fn.GenerateFunctions;
import tripleo.elijah.stages.gen_fn.GeneratedClass;
import tripleo.elijah.stages.gen_fn.GeneratedConstructor;
import tripleo.elijah.stages.gen_fn.GeneratedContainerNC;
import tripleo.elijah.stages.gen_fn.GeneratedFunction;
import tripleo.elijah.stages.gen_fn.GeneratedNamespace;
import tripleo.elijah.stages.gen_fn.GeneratedNode;
import tripleo.elijah.stages.gen_fn.GeneratedNodeVisit;
import tripleo.elijah.stages.gen_fn.GeneratedNodeVisitor;
import tripleo.elijah.stages.gen_fn.IdentTableEntry;
import tripleo.elijah.stages.instructions.IdentIA;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import static tripleo.elijah.util.Helpers.List_of;

public class PipelineLogicAction {
	private final PipelineLogic                pipelineLogic;
	private final DeducePhase.GeneratedClasses lgc;
	private final OS_Module                    mod;

	final @NotNull List<GeneratedNode> resolved_nodes = new ArrayList<GeneratedNode>();

	public PipelineLogicAction(final PipelineLogic aPipelineLogic, final OS_Module aMod, final DeducePhase.GeneratedClasses aLgc) {
		pipelineLogic = aPipelineLogic;
		mod           = aMod;
		lgc           = aLgc;
	}

	void one(final @NotNull List<EntryPoint> epl) {
		final GenerateFunctions gfm = pipelineLogic.getGenerateFunctions(mod);
		gfm.generateFromEntryPoints(epl, pipelineLogic.dp);
	}

	void two() throws Throwable {
		for (final GeneratedNode generatedNode : lgc) {
			if (!(generatedNode instanceof GNCoded))
				throw new IllegalStateException("node must be coded");

			GeneratedNodeVisit.visit(generatedNode, new GeneratedNodeVisitor() {
				@Override
				public void visitFunction(final GeneratedFunction aGeneratedFunction) throws Throwable {
					fa.accept(aGeneratedFunction);
				}

				@Override
				public void visitConstructor(final GeneratedConstructor aGeneratedConstructor) {

				}

				@Override
				public void visitNamespace(final GeneratedNamespace generatedNamespace) throws Throwable {
					ca.accept(generatedNamespace);

					for (GeneratedClass generatedClass : generatedNamespace.classMap.values()) {
						ca.accept(generatedClass);
					}
					for (GeneratedFunction generatedFunction : generatedNamespace.functionMap.values()) {
						for (IdentTableEntry identTableEntry : generatedFunction.idte_list) {
							if (identTableEntry.isResolved()) {
								GeneratedNode node = identTableEntry.resolvedType();
								resolved_nodes.add(node);
							} else if (generatedNode instanceof GeneratedNamespace) {
								final CAM cam = CAM.CAM_Factory.get((GeneratedNamespace) generatedNode);
								cam_accept(cam);
							}
						}
					}
				}

				private void cam_accept(final @NotNull CAM cam) throws Throwable {
					ca.accept(cam.one());

					final Iterable<GeneratedClass> two = cam.two();
					for (final GeneratedClass generatedClass : two) {
						ca.accept(generatedClass);
					}

					final Iterable<GeneratedFunction> three = cam.three();
					for (final GeneratedFunction generatedFunction1 : three) {
						for (final IdentTableEntry identTableEntry1 : generatedFunction1.idte_list) {
							if (identTableEntry1.isResolved()) {
								GeneratedNode node = identTableEntry1.resolvedType();
								resolved_nodes.add(node);
							}
						}
						break;
					}
				}

				@Override
				public void visitClass(final GeneratedClass generatedClass) throws Throwable {
					if (generatedClass.getCode() == 0) {
						assert false;
						ca.accept(generatedClass);
					}

					for (GeneratedClass generatedClass2 : generatedClass.classMap.values()) {
						if (generatedClass2.getCode() == 0)
							ca.accept(generatedClass2);
					}

					final Consumer<IdentTableEntry> cite = new CITE_Consumer(resolved_nodes, generatedNode, generatedClass);

					for (final GeneratedFunction generatedFunction : generatedClass.functionMap.values()) {
						for (IdentTableEntry identTableEntry : generatedFunction.idte_list) {
							cite.accept(identTableEntry);
						}

						// generatedFunction.idte_list
						// 		.stream()
						// 		.forEach(t -> {
						// 			try {
						// 				cite.accept(t);
						// 			} catch (Throwable aE) {
						// 				final ErrSink errSink = new StdErrSink();
						// 				errSink.exception((Exception) aE);
						// 				// assert false;
						// 				// throw new RuntimeException(aE);
						// 			}
						// 		});
					}
				}

				/*private*/ class CITE_Consumer implements Consumer<IdentTableEntry> {
					private final List<GeneratedNode> resolved_nodes;
					private final GeneratedNode  generatedNode;
					private final GeneratedClass generatedClass;

					public CITE_Consumer(final List<GeneratedNode> aResolved_nodes, final GeneratedNode aGeneratedNode, final GeneratedClass aGeneratedClass) {
						resolved_nodes = aResolved_nodes;
						generatedNode  = aGeneratedNode;
						generatedClass = aGeneratedClass;
					}

					@Override
					public void accept(final IdentTableEntry aIdentTableEntry) throws Throwable {
						accept(mod, resolved_nodes, generatedNode, generatedClass, aIdentTableEntry);
					}

					private void accept(final @NotNull OS_Module mod11,
										final @NotNull List<GeneratedNode> resolved_nodes,
										final @NotNull GeneratedNode generatedNode,
										final @NotNull GeneratedClass generatedClass,
										final @NotNull IdentTableEntry identTableEntry) throws Throwable {
						if (identTableEntry.isResolved()) {
							final GeneratedNode node = identTableEntry.resolvedType();
							resolved_nodes.add(node);
						}

						GeneratedNodeVisit.visit(generatedNode, new GeneratedNodeVisitor() {
							@Override
							public void visitFunction(final GeneratedFunction generatedFunction1) throws Throwable {
								fa.accept(generatedFunction1);
							}

							@Override
							public void visitConstructor(final GeneratedConstructor aGeneratedConstructor) {

							}

							@Override
							public void visitNamespace(final GeneratedNamespace aGeneratedNamespace) {

							}

							@Override
							public void visitClass(final GeneratedClass generatedClass1) {
								for (GeneratedClass generatedClass2 : generatedClass1.classMap.values()) {
									generatedClass2.setCode(mod11.getCompilation().nextClassCode());
								}

								for (GeneratedFunction generatedFunction1 : generatedClass.functionMap.values()) {
									for (IdentTableEntry identTableEntry1 : generatedFunction1.idte_list) {
										identTableEntry1.resolvedTypePromise().then(new DoneCallback<GeneratedNode>() {
											@Override
											public void onDone(final GeneratedNode result) {
												resolved_nodes.add(result);
											}
										});
									}
									break;
								}
							}
						});
					}
				}
			});
		}
	}

	void three() throws Throwable {
		for (final @NotNull GeneratedNode generatedNode : resolved_nodes) {
			set_code_for_node(generatedNode);
		}

		//resolved_nodes.forEach(this::set_code_for_node);
	}

	private void set_code_for_node(final @NotNull GeneratedNode generatedNode) throws Throwable {
		if (!(generatedNode instanceof GNCoded)) {
			throw new IllegalStateException("node is not coded");
		}

		final GNCoded coded = (GNCoded) generatedNode;
		set_code_for_coded(coded);
	}

	private void set_code_for_coded(final @NotNull GNCoded coded) throws Throwable {
		if (coded.getCode() != 0) {
			return;
		}

		switch (coded.getRole()) {
		case FUNCTION:
			fa.accept((GeneratedFunction) coded);
			break;
		case NAMESPACE:
		case CLASS:
			ca.accept((GeneratedContainerNC) coded);
			break;
		default:
			throw new IllegalStateException("Invalid coded role "+coded.getRole()+" is not FUNCTION, NAMESPACE or CLASS");
		}
	}

	void four() {
		pipelineLogic.dp.deduceModule(mod, lgc, pipelineLogic.getVerbosity());
	}

	void five() throws Throwable {
		resolveCheck(lgc);
	}

	void six() throws Throwable {
		@NotNull final List<GeneratedNode> lgf = List_of();

		for (final GeneratedNode gn : lgf) {
			GeneratedNodeVisiting.visit(gn, new AbstractGeneratedNodeVisitor() {
				@Override
				public void visitFunction(final GeneratedFunction gf) {
					System.out.println("----------------------------------------------------------");
					System.out.println(gf.name());
					System.out.println("----------------------------------------------------------");
					GeneratedFunction.printTables(gf);
					System.out.println("----------------------------------------------------------");
				}
			});
		}
	}

	void resolveCheck(final @NotNull DeducePhase.GeneratedClasses lgc) throws Throwable {
		for (final GeneratedNode generatedNode : lgc) {
			final ResolveCheck_GeneratedNodeVisitor generatedNodeVisitor = new ResolveCheck_GeneratedNodeVisitor();
			GeneratedNodeVisiting.visit(generatedNode, generatedNodeVisitor);
		}
	}

	public void act(final @NotNull List<EntryPoint> epl) throws Throwable {
		one(epl);
		two();
		three();
		four();
		five();
		six();
	}

	final         Consumer<GeneratedFunction>    fa = new Consumer<GeneratedFunction>() {
		@Override
		public void accept(final @NotNull GeneratedFunction aGeneratedFunction) throws Throwable {
			if (aGeneratedFunction.getCode() == 0)
				aGeneratedFunction.setCode(next());
		}

		private int next() {
			return mod.getCompilation().nextFunctionCode();
		}
	};
	final         Consumer<GeneratedContainerNC> ca = new Consumer<GeneratedContainerNC>() {
		@Override
		public void accept(final @NotNull GeneratedContainerNC aGeneratedContainerNC) throws Throwable {
			if (aGeneratedContainerNC.getCode() == 0)
				aGeneratedContainerNC.setCode(next());
		}

		private int next() {
			return mod.getCompilation().nextClassCode();
		}
	};

	private class ResolveCheck_GeneratedNodeVisitor extends AbstractGeneratedNodeVisitor {
		@Override
		public void visitFunction(final GeneratedFunction aGeneratedFunction) {
			// noop
		}

		@Override
		public void visitClass(final GeneratedClass generatedClass) {
			if (false) {
				for (GeneratedFunction generatedFunction : generatedClass.functionMap.values()) {
					for (IdentTableEntry identTableEntry : generatedFunction.idte_list) {
						final IdentIA ia2 = new IdentIA(identTableEntry.getIndex(), generatedFunction);
						final String  s   = generatedFunction.getIdentIAPathNormal(ia2);

						switch (identTableEntry/*.isResolved()*/.getStatus()) {
						case KNOWN:
							if (false) {
								final GeneratedNode node = identTableEntry.resolvedType();
								resolved_nodes.add(node);
							}

							System.out.println("91 Resolved IDENT " + s);
							break;
						default:
							if (false) {
								assert identTableEntry.getStatus() == BaseTableEntry.Status.UNKNOWN;
								identTableEntry.setStatus(BaseTableEntry.Status.UNKNOWN, null);
							}

							System.out.println("92 Unresolved IDENT " + s);
							break;
						}
					}
				}
			}
		}

		@Override
		public void visitNamespace(final GeneratedNamespace generatedNamespace) {
			if (false) {
				final NamespaceStatement namespaceStatement = generatedNamespace.getNamespaceStatement();

				for (GeneratedFunction generatedFunction : generatedNamespace.functionMap.values()) {
					for (IdentTableEntry identTableEntry : generatedFunction.idte_list) {
						if (identTableEntry.isResolved()) {
							final GeneratedNode node = identTableEntry.resolvedType();
							resolved_nodes.add(node);
						}
					}
				}
			}
		}
	}
}

class GeneratedNodeVisiting {
	static void visit(final @NotNull GeneratedNode aGeneratedNode,
					  final @NotNull GeneratedNodeVisitor v) throws Throwable {
		if (aGeneratedNode instanceof GeneratedFunction) {
			v.visitFunction((GeneratedFunction) aGeneratedNode);
		} else if (aGeneratedNode instanceof GeneratedNamespace) {
			v.visitNamespace((GeneratedNamespace) aGeneratedNode);
		} else if (aGeneratedNode instanceof GeneratedClass) {
			v.visitClass((GeneratedClass) aGeneratedNode);
		} else if (aGeneratedNode instanceof GeneratedConstructor) {
			v.visitConstructor((GeneratedConstructor) aGeneratedNode);
		} else
			throw new IllegalStateException("Unknown node type");
	}
}

//interface GeneratedNodeVisitor {
//	void visitFunction(final GeneratedFunction aGeneratedFunction);
//	void visitNamespace(final GeneratedNamespace aGeneratedNamespace);
//	void visitClass(final GeneratedClass aGeneratedClass);
//	void visitConstructor(final GeneratedConstructor aGeneratedConstructor);
//}

class AbstractGeneratedNodeVisitor implements GeneratedNodeVisitor {
	@Override
	public void visitFunction(final GeneratedFunction aGeneratedFunction) {}
	@Override
	public void visitNamespace(final GeneratedNamespace aGeneratedNamespace) {}
	@Override
	public void visitClass(final GeneratedClass aGeneratedClass) {}
	@Override
	public void visitConstructor(final GeneratedConstructor aGeneratedConstructor) {}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
