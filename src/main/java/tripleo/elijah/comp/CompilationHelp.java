/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp;

import tripleo.elijah.stages.gen_generic.GenerateResult;
import tripleo.elijah.stages.logging.ElLog;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import com.google.common.base.Preconditions;

import static tripleo.elijah.util.Helpers.List_of;

interface RuntimeProcess {
	void run(final Compilation aCompilation) throws Exception;

	void postProcess(final ProcessRecord aPr, final ICompilationAccess aCa);

	void prepare();
}

interface ICompilationAccess {
	void setPipelineLogic(final PipelineLogic pl);

	void addPipeline(final PipelineMember pl);

	ElLog.Verbosity testSilence();

	Compilation getCompilation();

	void writeLogs();

	Pipeline pipelines();
}

class StageToRuntime {
	@Contract("_, _, _ -> new")
	@NotNull
	public static RuntimeProcess get(final @NotNull Compilation.Stages stage, final ICompilationAccess ca, final ProcessRecord aPr) {
		switch (stage) {
		case E:
			return new EmptyProcess(ca, aPr);
		case O:
			return new OStageProcess(ca, aPr);
		case D:
			return new DStageProcess(ca, aPr);
		default:
			throw new IllegalStateException("No stage selected "+stage);
		}
	}
}

final class EmptyProcess implements RuntimeProcess {
	public EmptyProcess(final ICompilationAccess aCompilationAccess, final ProcessRecord aPr) {

	}

	@Override
	public void run(final Compilation aCompilation) {

	}

	@Override
	public void postProcess(final ProcessRecord aPr, final ICompilationAccess aCa) {

	}

	@Override
	public void prepare() {

	}
}

class DStageProcess implements RuntimeProcess {
	private final ICompilationAccess ca;
	private final ProcessRecord pr;

	public DStageProcess(final ICompilationAccess aCa, final ProcessRecord aPr) {
		ca = aCa;
		pr = aPr;
	}

	@Override
	public void run(final Compilation aCompilation) {
		int y=2;
	}

	@Override
	public void postProcess(final ProcessRecord aPr, final ICompilationAccess aCa) {
		assert pr.stage == Compilation.Stages.D;

		if (pr.stage != Compilation.Stages.E) {
			ca.writeLogs();
		}
	}

	@Override
	public void prepare() {

	}
}

class ProcessRecord {
	final @NotNull DeducePipeline     dpl;
	final @NotNull PipelineLogic      pipelineLogic;
	final @NotNull Compilation.Stages stage;

	public ProcessRecord(final @NotNull ICompilationAccess ca) {
		final Compilation compilation = ca.getCompilation();

		pipelineLogic = new PipelineLogic(ca);

		dpl           = new DeducePipeline(compilation);
		stage         = compilation.stage;
	}
}

class OStageProcess implements RuntimeProcess {
	private final ProcessRecord pr;
	private final ICompilationAccess ca;

	OStageProcess(final ICompilationAccess aCa, final ProcessRecord aPr) {
		ca = aCa;
		pr = aPr;
	}

	@Override
	public void prepare() {
		Preconditions.checkNotNull(pr);
		Preconditions.checkNotNull(pr.pipelineLogic);

		final GenerateResult generateResult = pr.pipelineLogic.gr;
		Preconditions.checkNotNull(generateResult);

		final Compilation        comp = ca.getCompilation();

		final DeducePipeline     dpl  = pr.dpl;
		final GeneratePipeline   gpl  = new GeneratePipeline(comp, dpl);
		final WritePipeline      wpl  = new WritePipeline(comp, generateResult);
		final WriteMesonPipeline wmpl = new WriteMesonPipeline(comp, generateResult, wpl);

		for (PipelineMember pipelineMember : List_of(gpl, dpl, wpl, wmpl)) {
			ca.addPipeline(pipelineMember);
		}
	}

	@Override
	public void run(final Compilation aCompilation) throws Exception {
		ca.pipelines().run();
	}

	@Override
	public void postProcess(final ProcessRecord aPr, final ICompilationAccess aCa) {
		if (pr.stage != Compilation.Stages.E) {
			ca.writeLogs();
		}
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
