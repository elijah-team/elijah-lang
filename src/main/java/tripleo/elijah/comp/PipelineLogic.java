/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.comp;

import tripleo.elijah.comp.pipeline_logic_actions.EBG_State;
import tripleo.elijah.comp.pipeline_logic_actions.PLA_AAGC;
import tripleo.elijah.comp.pipeline_logic_actions.PLA_DP_Finish;
import tripleo.elijah.comp.pipeline_logic_actions.PLA_PDA;
import tripleo.elijah.comp.pipeline_logic_actions.PLA_Run2;
import tripleo.elijah.comp.pipeline_logic_actions.PL_Action;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.deduce.DeducePhase;
import tripleo.elijah.stages.gen_c.GenerateC;
import tripleo.elijah.stages.gen_fn.BaseTableEntry;
import tripleo.elijah.stages.gen_fn.GenerateFunctions;
import tripleo.elijah.stages.gen_fn.GeneratePhase;
import tripleo.elijah.stages.gen_fn.GeneratedClass;
import tripleo.elijah.stages.gen_fn.GeneratedConstructor;
import tripleo.elijah.stages.gen_fn.GeneratedContainerNC;
import tripleo.elijah.stages.gen_fn.GeneratedFunction;
import tripleo.elijah.stages.gen_fn.GeneratedNamespace;
import tripleo.elijah.stages.gen_fn.GeneratedNode;
import tripleo.elijah.stages.gen_fn.GeneratedNodeVisit;
import tripleo.elijah.stages.gen_fn.GeneratedNodeVisitor;
import tripleo.elijah.stages.gen_fn.IdentTableEntry;
import tripleo.elijah.stages.gen_generic.GenerateResult;
import tripleo.elijah.stages.gen_generic.GenerateResultItem;
import tripleo.elijah.stages.instructions.IdentIA;
import tripleo.elijah.stages.logging.ElLog;
import tripleo.elijah.work.WorkManager;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * Created 12/30/20 2:14 AM
 */
public class PipelineLogic {
	public final  GeneratePhase   generatePhase;
	public final  DeducePhase     dp;
	public final  GenerateResult  gr                = new GenerateResult();
	public final  List<ElLog>     elLogs            = new LinkedList<ElLog>();
	final         List<OS_Module> mods              = new ArrayList<OS_Module>();
	private final ElLog.Verbosity verbosity;
	public        boolean         postDeduceEnabled = false;

	public PipelineLogic(final ElLog.Verbosity aVerbosity) {
		verbosity     = aVerbosity;
		//
		generatePhase = new GeneratePhase(this, verbosity);
		dp            = new DeducePhase(generatePhase, this, verbosity);
	}

	public PipelineLogic(final @NotNull ICompilationAccess ca) {
		this(ca.testSilence());
		ca.setPipelineLogic(this);
	}

	private final List<PL_Action> actions = new ArrayList<PL_Action>();

	protected void everythingBeforeGenerate(final @NotNull List<GeneratedNode> lgc) throws Throwable {
		final EBG_State st = new EBG_State(this, dp, mods, lgc);

		// create PL_Actions
		final @NotNull List<PL_Action> as = getActions(st);

		// add/inspect PL_Actions
		actions.addAll(as);
		int y=2;

		// run PL_Actions
		for (PL_Action action : actions) {
			action.run(this);
		}
	}

	private static @NotNull List<PL_Action> getActions(final @NotNull EBG_State st) {
		final List<PL_Action> a = new ArrayList<PL_Action>();

		// create PL_Actions
		st.mods().forEach(mod -> a.add(new PLA_Run2(mod)));

		final PLA_DP_Finish dpf      = new PLA_DP_Finish(st.dp());
		final PLA_AAGC      pla_aagc = new PLA_AAGC(st);
		final PLA_PDA       pla_pda  = new PLA_PDA(st);

		a.add(dpf);
		a.add(pla_aagc);
		a.add(pla_pda);

		return a;
	}

	public void generate(List<GeneratedNode> lgc) {
		final WorkManager wm = new WorkManager();

		for (OS_Module mod : mods) {
			final ErrSink        errSink   = mod.getCompilation().getErrSink();
			final GenerateC      generateC = new GenerateC(mod, errSink, verbosity, this);
			final GenerateResult ggr       = run3(mod, lgc, wm, generateC);

			wm.drain();
			gr.results().addAll(ggr.results());
		}
	}

	@NotNull
	public GenerateFunctions getGenerateFunctions(OS_Module mod) {
		return generatePhase.getGenerateFunctions(mod);
	}

	static class _X_PL_Run3 {
		GenerateResult run3(OS_Module mod, @NotNull List<GeneratedNode> lgc, WorkManager wm, GenerateC ggc) {
			final boolean newnew = false;

			final GenerateResult gr = new GenerateResult();

			for (GeneratedNode generatedNode : lgc) {
				if (generatedNode.module() != mod) continue; // README curious

				if (generatedNode instanceof GeneratedContainerNC) {
					final GeneratedContainerNC nc = (GeneratedContainerNC) generatedNode;

					nc.generateCode(ggc, gr);
					if (nc instanceof GeneratedClass) {
						if (newnew) {
/*
						gg(GenerateC::constructors_to_list_of_generated_nodes,
								//(GeneratedClass) generatedClass1 -> Class_constructors.apply(generatedClass1),
								//Class_constructors,
								nc1 -> extract_constructor_map(nc1),
								gr, wm, ggc
						);
*/
						} else {
							final GeneratedClass generatedClass = (GeneratedClass) nc;

							final @NotNull Collection<GeneratedNode>
									gn2 =
									GenerateC.constructors_to_list_of_generated_nodes(generatedClass.constructors.values());
							GenerateResult gr3 = ggc.generateCode(gn2, wm);
							gr.results().addAll(gr3.results());
						}
					}

					if (newnew) {
/*
					gg(GenerateC::functions_to_list_of_generated_nodes,
							nc1 -> extract_function_map(nc1),
							gr, wm, ggc
					);

					gg(GenerateC::classes_to_list_of_generated_nodes,
							PipelineLogic::extract_class_map,
							gr, wm, ggc
					);
*/
					} else {
						final @NotNull Collection<GeneratedNode> gn1 = GenerateC.functions_to_list_of_generated_nodes(nc.functionMap.values());
						GenerateResult gr2 = ggc.generateCode(gn1, wm);
						gr.results().addAll(gr2.results());
						final @NotNull Collection<GeneratedNode> gn2 = GenerateC.classes_to_list_of_generated_nodes(nc.classMap.values());
						GenerateResult gr3 = ggc.generateCode(gn2, wm);
						gr.results().addAll(gr3.results());
					}
				} else {
					System.out.println("2009 " + generatedNode.getClass().getName());
				}
			}

			return gr;
		}
	}

	protected GenerateResult run3(OS_Module mod, @NotNull List<GeneratedNode> lgc, WorkManager wm, GenerateC ggc) {
		final _X_PL_Run3 xplr = new _X_PL_Run3();
		final GenerateResult gr1 = xplr.run3(mod, lgc, wm, ggc);
//		return gr1;

		final GenerateResult gr = new GenerateResult();

		for (GeneratedNode generatedNode : lgc) {
			if (generatedNode.module() != mod) continue; // README curious

			if (generatedNode instanceof GeneratedContainerNC) {
				final GeneratedContainerNC nc = (GeneratedContainerNC) generatedNode;

				nc.generateCode(ggc, gr);
				if (nc instanceof GeneratedClass) {
					final GeneratedClass generatedClass = (GeneratedClass) nc;

					run3_work(GenerateC::constructors_to_list_of_generated_nodes,
							generatedClass.constructors.values(),
							gr.results(),
							wm, ggc);
				}

				run3_work(GenerateC::functions_to_list_of_generated_nodes,
						nc.functionMap.values(),
						gr.results(),
						wm, ggc);
				run3_work(GenerateC::classes_to_list_of_generated_nodes,
						nc.classMap.values(),
						gr.results(),
						wm, ggc);
			} else {
				System.out.println("2009 " + generatedNode.getClass().getName());
			}
		}

		return gr;
	}

	private <T> void run3_work(final @NotNull Function<Collection<T>, @NotNull Collection<GeneratedNode>> f,
							   final Collection<T> values,
							   final @NotNull List<GenerateResultItem> collector,
							   final WorkManager wm,
							   final @NotNull GenerateC ggc) {
		final @NotNull Collection<GeneratedNode> gn2     = f.apply(values);
		final @NotNull GenerateResult            gr3     = ggc.generateCode(gn2, wm);
		final @NotNull List<GenerateResultItem>  results = gr3.results();

		collector.addAll(results);
	}

	private <F> void gg(final Function<F, Collection<F>> ff,
					   final Function<GeneratedClass, Collection<F>> i,
					   final GenerateResult aGr,
					   final WorkManager wm,
					   final GenerateC ggc) {
		final Supplier<Collection<F>> ii = new Supplier<Collection<F>>() {
			@Override
			public Collection<F> get() {
				return i.apply(null);
			}
		};

		//g(ff, ii, aGr, aWm, aGgc);
		final @NotNull Collection<F> gn1 = ff.apply((F) ii.get());
		GenerateResult gr2 = ggc.generateCode((Collection<GeneratedNode>) gn1, wm);
		aGr.results().addAll(gr2.results());
	}

	protected <F> void g(final @NotNull Function<F, Collection<GeneratedNode>> ff,
						 final @NotNull Supplier<F> i,
						 //final @NotNull Function<F, Collection<GeneratedConstructor>> i,
						 final @NotNull GenerateResult aGr,
						 final @NotNull WorkManager aWorkManager,
						 final @NotNull GenerateC ggc) {
		//@NotNull final Collection<GeneratedNode> xx =
		//		GenerateC.functions_to_list_of_generated_nodes(nc.functionMap.values());
		final @NotNull Collection<GeneratedNode> gn1 = ff.apply(i.get());
		GenerateResult gr2 = ggc.generateCode(gn1, aWorkManager);
		aGr.results().addAll(gr2.results());
	}

	public void addModule(OS_Module m) {
		mods.add(m);
	}

	public void resolveCheck(final @NotNull DeducePhase.GeneratedClasses lgc) {
		final List<GeneratedNode> resolved_nodes = new ArrayList<GeneratedNode>();

		for (final GeneratedNode generatedNode : lgc) {
			try {
				GeneratedNodeVisit.visit(generatedNode, new GeneratedNodeVisitor() {
					@Override
					public void visitFunction(final GeneratedFunction aGeneratedFunction) {

					}

					@Override
					public void visitConstructor(final GeneratedConstructor aGeneratedConstructor) {

					}

					@Override
					public void visitNamespace(final GeneratedNamespace generatedNamespace) {
						if (false) {
							for (GeneratedFunction generatedFunction : generatedNamespace.functionMap.values()) {
								for (IdentTableEntry identTableEntry : generatedFunction.idte_list) {
									if (identTableEntry.isResolved()) {
										GeneratedNode node = identTableEntry.resolvedType();
										resolved_nodes.add(node);
									}
								}
							}
						}
					}

					@Override
					public void visitClass(final GeneratedClass generatedClass) {
						if (false)
							for (GeneratedFunction generatedFunction : generatedClass.functionMap.values()) {
								for (IdentTableEntry identTableEntry : generatedFunction.idte_list) {
									final IdentIA ia2 = new IdentIA(identTableEntry.getIndex(), generatedFunction);
									final String s = generatedFunction.getIdentIAPathNormal(ia2);
									if (identTableEntry/*.isResolved()*/.getStatus() == BaseTableEntry.Status.KNOWN) {
										GeneratedNode node = identTableEntry.resolvedType();
										resolved_nodes.add(node);
										System.out.println("91 Resolved IDENT "+ s);
									} else {
										assert identTableEntry.getStatus() == BaseTableEntry.Status.UNKNOWN;
										identTableEntry.setStatus(BaseTableEntry.Status.UNKNOWN, null);
										System.out.println("92 Unresolved IDENT "+ s);
									}
								}
							}
					}
				});
			} catch (Throwable aE) {
				throw new RuntimeException(aE);
			}
		}
	}

	public void addLog(ElLog aLog) {
		elLogs.add(aLog);
	}

	public ElLog.Verbosity getVerbosity() {
		return verbosity;
	}

	public static void debug_buffers(final @NotNull GenerateResult gr, final @NotNull PrintStream stream) {
		for (final GenerateResultItem ab : gr.results()) {
			stream.println("---------------------------------------------------------------");
			stream.println(ab.counter);
			stream.println(ab.ty);
			stream.println(ab.output);
			stream.println(ab.node.identityString());
			stream.println(ab.buffer.getText());
			stream.println("---------------------------------------------------------------");
		}
	}

	@Contract(pure = true)
	private static @NotNull Collection<GeneratedConstructor> extract_constructor_map(final @NotNull GeneratedClass nc1) {
		return nc1.constructors.values();
	}

	@Contract(pure = true)
	private static @NotNull Collection<GeneratedClass> extract_class_map(@NotNull GeneratedClass nc1) {
		return nc1.classMap.values();
	}

	@Contract(pure = true)
	private static @NotNull Collection<GeneratedFunction> extract_function_map(@NotNull GeneratedClass nc1) {
		return nc1.functionMap.values();
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
