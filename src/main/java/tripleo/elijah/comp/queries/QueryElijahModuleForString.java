package tripleo.elijah.comp.queries;

import antlr.RecognitionException;
import antlr.TokenStreamException;
import tripleo.elijah.Out;
import tripleo.elijah.comp.Compilation;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijjah.ElijjahLexer;
import tripleo.elijjah.ElijjahParser;

import java.io.StringReader;

public class QueryElijahModuleForString {
	private Exception exc;
	private OS_Module mod;

	void parseString(String sourceText, String filename, Compilation compilation) {
		ElijjahParser      parser       = null;
		boolean            do_out       = false;
		final StringReader stringReader = new StringReader(sourceText);
		try {
			final ElijjahLexer lexer = new ElijjahLexer(stringReader);
			lexer.setFilename(filename);
			parser     = new ElijjahParser(lexer);
			parser.out = new Out(filename, compilation, do_out);
			parser.setFilename(filename);
			try {
				parser.program();
			} catch (RecognitionException aE) {
				exc = aE;
				return;
			} catch (TokenStreamException aE) {
				exc = aE;
				return;
			}
			mod = parser.out.module();
		} finally {
			if (parser != null)
				parser.out = null;
			stringReader.close();
		}
	}

	OS_Module getResult() {
		return mod;
	}

	Exception getError() {
		return exc;
	}
}
