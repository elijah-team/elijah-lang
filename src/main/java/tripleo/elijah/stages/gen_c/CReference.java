/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_c;

import org.jetbrains.annotations.Contract;
import com.google.common.base.Preconditions;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tripleo.elijah.lang.AliasStatement;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.ConstructorDef;
import tripleo.elijah.lang.DecideElObjectType;
import tripleo.elijah.lang.DefFunctionDef;
import tripleo.elijah.lang.FunctionDef;
import tripleo.elijah.lang.NamespaceStatement;
import tripleo.elijah.lang.OS_Element;
import tripleo.elijah.lang.PropertyStatement;
import tripleo.elijah.lang.VariableStatement;
import tripleo.elijah.stages.deduce.DeduceTypes2.IALogic;
import tripleo.elijah.stages.deduce.DeduceTypes2.IALogicProcessable;
import tripleo.elijah.stages.deduce.FunctionInvocation;
import tripleo.elijah.stages.gen_fn.BaseGeneratedFunction;
import tripleo.elijah.stages.gen_fn.GeneratedClass;
import tripleo.elijah.stages.gen_fn.GeneratedConstructor;
import tripleo.elijah.stages.gen_fn.GeneratedContainerNC;
import tripleo.elijah.stages.gen_fn.GeneratedFunction;
import tripleo.elijah.stages.gen_fn.GeneratedNamespace;
import tripleo.elijah.stages.gen_fn.GeneratedNode;
import tripleo.elijah.stages.gen_fn.IdentTableEntry;
import tripleo.elijah.stages.gen_fn.ProcTableEntry;
import tripleo.elijah.stages.gen_fn.VariableTableEntry;
import tripleo.elijah.stages.instructions.IdentIA;
import tripleo.elijah.stages.instructions.InstructionArgument;
import tripleo.elijah.stages.instructions.IntegerIA;
import tripleo.elijah.stages.instructions.ProcIA;
import tripleo.elijah.util.Helpers;
import tripleo.elijah.util.NotImplementedException;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static tripleo.elijah.stages.deduce.DeduceTypes2.to_int;

/**
 * Created 1/9/21 7:12 AM
 */
public class CReference {
	private String rtext = null;
	private List<String> args;
	private List<Reference> refs;

	static class Reference {
		final String              text;
		final Ref                 type;
		final String              value;

		public Reference(final String aText, final Ref aType, final String aValue) {
			text = aText;
			type = aType;
			value = aValue;
		}

		public Reference(final String aText, final Ref aType) {
			text = aText;
			type = aType;
			value = null;
		}

		public void buildHelper(final BuildState st) {
			type.buildHelper(this, st);
		}
	}

	enum Ref {
		// https://www.baeldung.com/a-guide-to-java-enums
		LOCAL {
			@Override
			public void buildHelper(final Reference ref, final BuildState sb) {
				String text = "vv" + ref.text;
				sb.appendText(text, false);
			}
		},
		MEMBER {
			@Override
			public void buildHelper(final Reference ref, final BuildState sb) {
				String text = "->vm" + ref.text;

				final StringBuilder sb1 = new StringBuilder();

				sb1.append(text);
				if (ref.value != null) {
					sb1.append(" = ");
					sb1.append(ref.value);
					sb1.append(";");
				}

				sb.appendText(sb1.toString(), false);
			}
		},
		PROPERTY_GET {
			@Override
			public void buildHelper(final Reference ref, final BuildState sb) {
				String text;
				final String s = sb.toString();
				text = String.format("%s%s)", ref.text, s);
				sb.open = false;
//				if (!s.equals(""))
				sb.needs_comma = false;
				sb.appendText(text, true);
			}
		},
		PROPERTY_SET {
			@Override
			public void buildHelper(final Reference ref, final BuildState sb) {
				String text;
				final String s = sb.toString();
				text = String.format("%s%s, %s);", ref.text, s, ref.value);
				sb.open = false;
//				if (!s.equals(""))
				sb.needs_comma = false;
				sb.appendText(text, true);
			}
		},
		INLINE_MEMBER {
			@Override
			public void buildHelper(final Reference ref, final BuildState sb) {
				String text = Emit.emit("/*219*/") + ".vm" + ref.text;
				sb.appendText(text, false);
			}
		},
		CONSTRUCTOR {
			@Override
			public void buildHelper(final Reference ref, final BuildState sb) {
				String text;
				final String s = sb.toString();
				text = String.format("%s(%s", ref.text, s);
				sb.open = false;
				if (!s.equals("")) sb.needs_comma = true;
				sb.appendText(text + ")", true);
			}
		},
		DIRECT_MEMBER {
			@Override
			public void buildHelper(final Reference ref, final BuildState sb) {
				String text;
				text = Emit.emit("/*124*/")+"vsc->vm" + ref.text;

				final StringBuilder sb1 = new StringBuilder();

				sb1.append(text);
				if (ref.value != null) {
					sb1.append(" = ");
					sb1.append(ref.value);
					sb1.append(";");
				}

				sb.appendText(sb1.toString(), false);
			}
		},
		LITERAL {
			@Override
			public void buildHelper(final Reference ref, final BuildState sb) {
				String text = ref.text;
				sb.appendText(text, false);
			}
		},
		FUNCTION {
			@Override
			public void buildHelper(final Reference ref, final BuildState sb) {
				String text;
				final String s = sb.asString();
				text = String.format("%s(%s", ref.text, s);
				sb.open = true;
				if (!s.equals("")) sb.needs_comma = true;
				sb.appendText(text, true);
			}
		};

		public abstract void buildHelper(final Reference ref, final BuildState sb);
	}

	void addRef(String text, Ref type) {
		refs.add(new Reference(text, type));
	}

	final static class __getIdentIAPath_State {
		int i;
		String text;
	}

	void addRef(String text, Ref type, String aValue) {
		refs.add(new Reference(text, type, aValue));
	}

	final static class __getIdentIAPath_State2 {
		private final List<InstructionArgument>    s;
		private final Generate_Code_For_Method.AOG aog;
		private final String                       aValue;
		public boolean skip_to_next;
		String    text;
		Reference ref;

		public __getIdentIAPath_State2(final Generate_Code_For_Method.AOG aAog, final String aAValue, final List<InstructionArgument> aS) {
			aog    = aAog;
			aValue = aAValue;
			s      = aS;
		}
	}

	final static class __getIdentIAPath_Process {
		private final List<InstructionArgument>         s;
		private final ArrayList<Reference>              refs;
		private final __getIdentIAPath_State2[]         st2;
		private final __getIdentIAPath_Process_Single[] processes;
		private final List<String>                      sl;

		public __getIdentIAPath_Process(final List<InstructionArgument> aS, final Generate_Code_For_Method.AOG aAog, final String aValue, final BaseGeneratedFunction aGeneratedFunction) {
			s     = aS;

			final int capacity = s.size();

			sl        = new ArrayList<String>(capacity);
			refs      = new ArrayList<Reference>(capacity);
			st2       = new __getIdentIAPath_State2[capacity];
			processes = new __getIdentIAPath_Process_Single[capacity];

			for (int i = 0; i < capacity; i++) {
				st2[i]       = new __getIdentIAPath_State2(aAog, aValue, s);
				processes[i] = new __getIdentIAPath_Process_Single(s.get(i), i, st2[i], sl, aGeneratedFunction);
			}
		}

		public void process(final CReference cr) {
			for (int i = 0; i < processes.length; i++) {
				final __getIdentIAPath_Process_Single p = processes[i];
				st2[i] = p.process(cr);

				final String text = st2[i].text;
				if (text != null)
					sl.add(text);
			}
		}
	}

	final static class __getIdentIAPath_Process_Single {
		private final InstructionArgument     ia;
		private final int                     index;
		private final __getIdentIAPath_State2 st;
		private final List<String>            sl;
		private final BaseGeneratedFunction   generatedFunction;

		@Contract(pure = true)
		public __getIdentIAPath_Process_Single(final InstructionArgument aInstructionArgument,
											   final int aIndex,
											   final __getIdentIAPath_State2 givenState,
											   final List<String> aSl,
											   final BaseGeneratedFunction aGeneratedFunction) {
			ia                = aInstructionArgument;
			index             = aIndex;
			st                = givenState;
			sl                = aSl;
			generatedFunction = aGeneratedFunction;
		}

		public __getIdentIAPath_State2 process(final CReference cr) {
			final String[] text = new String[1];

			IALogic.process(ia, new IALogicProcessable() {
				@Override
				public void doIntegerIA(final IntegerIA aIntegerIA) {
					// should only be the first element if at all
					assert index == 0;
					final VariableTableEntry vte = aIntegerIA.getEntry();
					text[0] = "vv" + vte.getName();
					addRef(vte.getName(), Ref.LOCAL);
				}

				@Override
				public void doIdentIA(final IdentIA aIdentIA) {
					boolean               skip             = false;
					final IdentTableEntry idte             = aIdentIA.getEntry();
					final OS_Element      resolved_element = idte.getResolvedElement();

					if (resolved_element != null) {
						GeneratedNode resolved = null;

						final int                          sSize  = st.s.size();
						final Generate_Code_For_Method.AOG aog    = st.aog;
						final String                       aValue = st.aValue;

						switch (DecideElObjectType.getElObjectType(resolved_element)) {
						case CLASS:
							resolved = identIA_resolves_to_CLASS(idte);
							break;
						case FUNCTION:
							resolved = identIA_resolves_to_FUNCTION(idte);
							break;
						case PROPERTY:
							skip = identIA_resolves_to_PROPERTY(idte, sSize, aog, aValue);
							break;
						}

						PROEPRTY_part_deux(skip, idte, resolved_element, resolved, sSize, aValue);
					} else {
						null_resolved_element(aIdentIA, idte);
					}
				}

				private void null_resolved_element(final IdentIA aIdentIA, final IdentTableEntry idte) {
					final String path = generatedFunction.getIdentIAPathNormal(aIdentIA);

					switch (aIdentIA.getEntry().getStatus()) {
					case KNOWN:
						assert false;
						break;
					case UNCHECKED:
						final String path2 = idte.getIdent().getText();
						final String text3 = String.format("<<UNCHECKED ia2: %s>>", path);
						st.text = text3;
//						assert false;
						break;
					case UNKNOWN:
						final String text1 = idte.getIdent().getText();

//							assert false;
						// TODO make tests pass but I dont like this (should emit a dummy function or placeholder)
						if (sl.size() == 0) {
							st.text =
									Emit.emit("/*149*/") + text1; // TODO check if it belongs somewhere else (what does this mean?)
						} else {
							st.text = Emit.emit("/*152*/") + "vm" + text1;
						}
						System.err.println("119 " + idte.getIdent() + " " + idte.getStatus());
						String text2 = (Emit.emit("/*114*/") + String.format("%s is UNKNOWN", text1));
						addRef(text2, Ref.MEMBER);
						break;
					default:
						throw new IllegalStateException("Unexpected value: " + aIdentIA.getEntry().getStatus());
					}
				}

				private void PROEPRTY_part_deux(final boolean skip,
												final IdentTableEntry idte,
												final OS_Element resolved_element,
												final GeneratedNode resolved,
												final int sSize,
												final String aValue) {
					if (!skip) {
						final short state2;

						if (idte.externalRef != null)
							state2 = 2;
						else
							state2 = 1;

						switch (state2) {
						case 1:
							if (resolved == null) {
								System.err.println("***88*** resolved is null for " + idte);
							}
							if (sSize >= index + 1) {
								_getIdentIAPath_IdentIAHelper_State hs = new _getIdentIAPath_IdentIAHelper_State(null,
												sl,
												index,
												sSize,
												resolved_element,
												generatedFunction,
												resolved,
												aValue);
								_getIdentIAPath_IdentIAHelper(hs);
								boolean dummy = hs.result;
								text[0] = null;
							} else {
								_getIdentIAPath_IdentIAHelper_State hs = new _getIdentIAPath_IdentIAHelper_State(st.s.get(index + 1),
										sl,
										index,
										sSize,
										resolved_element,
										generatedFunction,
										resolved,
										aValue);
								_getIdentIAPath_IdentIAHelper(hs);
								st.skip_to_next = hs.result;
								int y = 2;
							}
							break;
						case 2:
							if ((resolved_element instanceof VariableStatement)) {
								final String text2 = ((VariableStatement) resolved_element).getName();

								final GeneratedNode externalRef = idte.externalRef;
								if (externalRef instanceof GeneratedNamespace) {
									final String
											text3 =
											String.format("zN%d_instance", ((GeneratedNamespace) externalRef).getCode());
									addRef(text3, Ref.LITERAL, null);
								} else if (externalRef instanceof GeneratedClass) {
									assert false;
									final String
											text3 =
											String.format("zN%d_instance", ((GeneratedClass) externalRef).getCode());
									addRef(text3, Ref.LITERAL, null);
								} else
									throw new IllegalStateException();
								addRef(text2, Ref.MEMBER, aValue);
							} else
								throw new NotImplementedException();
							break;
						}
					}
				}

				private boolean identIA_resolves_to_PROPERTY(final IdentTableEntry idte, final int sSize, final Generate_Code_For_Method.AOG aog, final String aValue) {
					NotImplementedException.raise();

					boolean       skip      = false;
					GeneratedNode resolved1 = idte.type.resolved();
					int           code;

					if (resolved1 != null)
						code = ((GeneratedContainerNC) resolved1).getCode();
					else
						code = -1;
					short state = 0;
					if (index < sSize - 1) {
						state = 1;
					} else {
						switch (aog) {
						case GET:
							state = 1;
							break;
						case ASSIGN:
							state = 2;
							break;
						}
					}
					switch (state) {
					case 1:
						addRef(String.format("ZP%d_get%s(", code, idte.getIdent().getText()), Ref.PROPERTY_GET);
						skip = true;
						text[0] = null;
						break;
					case 2:
						addRef(String.format("ZP%d_set%s(", code, idte.getIdent().getText()), Ref.PROPERTY_SET, aValue);
						skip = true;
						text[0] = null;
						break;
					default:
						throw new IllegalStateException("Unexpected value: " + state);
					}
//					return;
					return skip;
				}

				private GeneratedNode identIA_resolves_to_CLASS(final @NotNull IdentTableEntry idte) {
					GeneratedNode resolved = null;

					if (idte.type != null)
						resolved = idte.type.resolved();
					if (resolved == null)
						resolved = idte.resolvedType();

					return resolved;
				}

				@Nullable
				private GeneratedNode identIA_resolves_to_FUNCTION(final @NotNull IdentTableEntry idte) {
					@Nullable ProcTableEntry pte = idte.getCallablePTE();

					GeneratedNode resolved = null;

					if (pte != null) {
						FunctionInvocation fi = pte.getFunctionInvocation();
						if (fi != null) {
							BaseGeneratedFunction gen = fi.getGenerated();
							if (gen != null)
								resolved = gen;
						}
					}
					if (resolved == null) {
						GeneratedNode resolved1 = idte.resolvedType();
						if (resolved1 instanceof GeneratedFunction)
							resolved = resolved1;
						else if (resolved1 instanceof GeneratedClass)
							resolved = resolved1;
					}
					return resolved;
				}

				@Override
				public void doProcIA(final ProcIA aProcIA) {
					final ProcTableEntry prte = aProcIA.getEntry();
					st.text = getIdentIAPath_Proc(prte);
				}

				public String getIdentIAPath_Proc(@NotNull ProcTableEntry aPrte) {
					final String                text;
					final BaseGeneratedFunction generated = aPrte.getFunctionInvocation().getGenerated();

					if (generated == null)
						throw new IllegalStateException();

					final GeneratedContainerNC genClass = (GeneratedContainerNC) generated.getGenClass();

					if (generated instanceof GeneratedConstructor) {
						int          y                   = 2;
						final String constructorNameText = generated.getFunctionName();

						text = String.format("ZC%d%s", genClass.getCode(), constructorNameText);
						addRef(text, Ref.CONSTRUCTOR);
					} else {
						text = String.format("Z%d%s", genClass.getCode(), generated.getFunctionName());
						addRef(text, Ref.FUNCTION);
					}

					return text;
				}

				void addRef(final String aText, final Ref aType) {
					st.ref = new Reference(aText, aType);
				}

				void addRef(final String aText, final Ref aType, final String aValue) {
					st.ref = new Reference(aText, aType, aValue);
				}

				private void _getIdentIAPath_IdentIAHelper(final _getIdentIAPath_IdentIAHelper_State aHelperState) {
					aHelperState.result = _getIdentIAPath_IdentIAHelper(
							aHelperState.ia_next,
							aHelperState.sl,
							aHelperState.index,
							aHelperState.sSize,
							aHelperState.resolved_element,
							aHelperState.generatedFunction,
							aHelperState.resolved,
							aHelperState.aValue
					);
				}

				private boolean _getIdentIAPath_IdentIAHelper(final InstructionArgument aInstructionArgument,
															  final List<String> aSl,
															  final int aIndex,
															  final int aSSize,
															  final OS_Element aResolved_element,
															  final BaseGeneratedFunction aGeneratedFunction,
															  final GeneratedNode aResolved,
															  final String aAValue) {
					return false;
				}
			});

			return st;
		}
	}

	final static class _getIdentIAPath_IdentIAHelper_State {
		public boolean             result;
		public InstructionArgument ia_next;
		public List<String>        sl;
		public int                 index;
		public int                 sSize;
		public OS_Element          resolved_element;
		public BaseGeneratedFunction generatedFunction;
		public GeneratedNode         resolved;
		public String                aValue;
		public _getIdentIAPath_IdentIAHelper_State(final InstructionArgument aIa_next,
												   final List<String> aSl,
												   final int aIndex,
												   final int aSSize,
												   final OS_Element aResolved_element,
												   final BaseGeneratedFunction aGeneratedFunction,
												   final GeneratedNode aResolved,
												   final String aAValue) {
			ia_next           = aIa_next;
			sl                = aSl;
			index             = aIndex;
			sSize             = aSSize;
			resolved_element  = aResolved_element;
			generatedFunction = aGeneratedFunction;
			resolved          = aResolved;
			aValue            = aAValue;
		}
	}

	public String getIdentIAPath(final @NotNull IdentIA ia2,
								 final BaseGeneratedFunction generatedFunction,
								 final Generate_Code_For_Method.AOG aog,
								 final String aValue) {
		assert ia2.gf == generatedFunction;
		final List<InstructionArgument> s = _getIdentIAPathList(ia2);
		refs = new ArrayList<Reference>(s.size());

		__getIdentIAPath_State  st  = new __getIdentIAPath_State();

		__getIdentIAPath_Process sp = new __getIdentIAPath_Process(s, aog, aValue, generatedFunction);
		sp.process(this);
		final String as = Arrays.toString(sp.st2);
		System.out.println(as);

		//
		// TODO NOT LOOKING UP THINGS, IE PROPERTIES, MEMBERS
		//
		List<String> sl = new ArrayList<String>();
		for (int i = 0, sSize = s.size(); i < sSize; i++) {
			InstructionArgument ia = s.get(i);

			st.i = i;

			IALogic.process(ia, new IALogicProcessable() {
				@Override
				public void doIntegerIA(final IntegerIA aIntegerIA) {
					// should only be the first element if at all
					assert st.i == 0;
					final VariableTableEntry vte = aIntegerIA.getEntry();
					st.text = "vv" + vte.getName();
					addRef(vte.getName(), Ref.LOCAL);
				}

				@Override
				public void doIdentIA(final IdentIA aIdentIA) {
					boolean skip = false;
					final IdentTableEntry idte = ((IdentIA)ia).getEntry();
					OS_Element resolved_element = idte.getResolvedElement();
					if (resolved_element != null) {
						GeneratedNode resolved = null;

						switch (DecideElObjectType.getElObjectType(resolved_element)) {
						case CLASS:
							if (idte.type != null)
								resolved = idte.type.resolved();
							if (resolved == null)
								resolved = idte.resolvedType();
							break;
						case FUNCTION:
							@Nullable ProcTableEntry pte = idte.getCallablePTE();
							if (pte != null) {
								FunctionInvocation fi = pte.getFunctionInvocation();
								if (fi != null) {
									BaseGeneratedFunction gen = fi.getGenerated();
									if (gen != null)
										resolved = gen;
								}
							}
							if (resolved == null) {
								GeneratedNode resolved1 = idte.resolvedType();
								if (resolved1 instanceof GeneratedFunction)
									resolved = resolved1;
								else if (resolved1 instanceof GeneratedClass)
									resolved = resolved1;
							}
							break;
						case PROPERTY:
							NotImplementedException.raise();
							GeneratedNode resolved1 = idte.type.resolved();
							int code;
							if (resolved1 != null)
								code = ((GeneratedContainerNC) resolved1).getCode();
							else
								code = -1;
							short state = 0;
							if (st.i < sSize-1) {
								state = 1;
							} else {
								switch (aog) {
								case GET:
									state =1;
									break;
								case ASSIGN:
									state =2;
									break;
								}
							}
							switch (state) {
							case 1:
								addRef(String.format("ZP%d_get%s(", code, idte.getIdent().getText()), Ref.PROPERTY_GET);
								skip = true;
								st.text = null;
								break;
							case 2:
								addRef(String.format("ZP%d_set%s(", code, idte.getIdent().getText()), Ref.PROPERTY_SET, aValue);
								skip = true;
								st.text = null;
								break;
							default:
								throw new IllegalStateException("Unexpected value: " + state);
							}
							break;
						}

						if (!skip) {
							short state = 1;
							if (idte.externalRef != null) {
								state = 2;
							}
							switch (state) {
							case 1:
								if (resolved == null) {
									System.err.println("***88*** resolved is null for "+idte);
								}
								if (sSize >= st.i + 1) {
									_getIdentIAPath_IdentIAHelper(null, sl, st.i, sSize, resolved_element, generatedFunction, resolved, aValue);
									st.text = null;
								} else {
									boolean b = _getIdentIAPath_IdentIAHelper(s.get(st.i + 1), sl, st.i, sSize, resolved_element, generatedFunction, resolved, aValue);
									if (b) st.i++;
								}
								break;
							case 2:
								if ((resolved_element instanceof VariableStatement)) {
									final String text2 = ((VariableStatement) resolved_element).getName();

									final GeneratedNode externalRef = idte.externalRef;
									if (externalRef instanceof GeneratedNamespace) {
										final String text3 = String.format("zN%d_instance", ((GeneratedNamespace) externalRef).getCode());
										addRef(text3, Ref.LITERAL, null);
									} else if (externalRef instanceof GeneratedClass) {
										assert false;
										final String text3 = String.format("zN%d_instance", ((GeneratedClass) externalRef).getCode());
										addRef(text3, Ref.LITERAL, null);
									} else
										throw new IllegalStateException();
									addRef(text2, Ref.MEMBER, aValue);
								} else
									throw new NotImplementedException();
								break;
							}
						}
					} else {
						switch (ia2.getEntry().getStatus()) {
						case KNOWN:
							assert false;
							break;
						case UNCHECKED:
							final String path2 = generatedFunction.getIdentIAPathNormal(ia2);
							final String text3 = String.format("<<UNCHECKED ia2: %s>>", path2/*idte.getIdent().getText()*/);
							st.text = text3;
//						assert false;
							break;
						case UNKNOWN:
							final String path = generatedFunction.getIdentIAPathNormal(ia2);
							final String text1 = idte.getIdent().getText();
//						assert false;
							// TODO make tests pass but I dont like this (should emit a dummy function or placeholder)
							if (sl.size() == 0) {
								st.text = Emit.emit("/*149*/") + text1; // TODO check if it belongs somewhere else (what does this mean?)
							} else {
								st.text = Emit.emit("/*152*/") + "vm" + text1;
							}
							System.err.println("119 " + idte.getIdent() + " " + idte.getStatus());
							String text2 = (Emit.emit("/*114*/") + String.format("%s is UNKNOWN", text1));
							addRef(text2, Ref.MEMBER);
							break;
						default:
							throw new IllegalStateException("Unexpected value: " + ia2.getEntry().getStatus());
						}
					}
				}

				@Override
				public void doProcIA(final ProcIA aProcIA) {
					final ProcTableEntry prte = generatedFunction.getProcTableEntry(to_int(ia));
					st.text = getIdentIAPath_Proc(prte);
				}
			});

			i    = st.i;
			//text = st.text;

			if (st.text != null)
				sl.add(st.text);
		}
		if (sl.size() == 0)
			rtext = "";
		else
			rtext = Helpers.String_join(".", sl);
		return rtext;
	}

	public String getIdentIAPath_Proc(@NotNull ProcTableEntry aPrte) {
		final String text;
		final BaseGeneratedFunction generated = aPrte.getFunctionInvocation().getGenerated();

		if (generated == null)
			throw new IllegalStateException();

		final GeneratedContainerNC genClass = (GeneratedContainerNC) generated.getGenClass();

		if (generated instanceof GeneratedConstructor) {
			int y = 2;
			final String constructorNameText = generated.getFunctionName();

			text = String.format("ZC%d%s", genClass.getCode(), constructorNameText);
			addRef(text, Ref.CONSTRUCTOR);
		} else {
			text = String.format("Z%d%s", genClass.getCode(), generated.getFunctionName());
			addRef(text, Ref.FUNCTION);
		}

		return text;
	}

	boolean _getIdentIAPath_IdentIAHelper(InstructionArgument ia_next,
										  List<String> sl,
										  int i,
										  int sSize,
										  OS_Element resolved_element,
										  BaseGeneratedFunction generatedFunction,
										  GeneratedNode aResolved,
										  final String aValue) {
		boolean b = false;
		if (resolved_element instanceof ClassStatement) {
			// Assuming constructor call
			int code;
			if (aResolved != null) {
				code = ((GeneratedContainerNC)aResolved).getCode();
			} else {
				code = -1;
				System.err.println("** 31116 not resolved "+resolved_element);
			}
			// README might be calling reflect or Type or Name
			// TODO what about named constructors -- should be called with construct keyword
			if (ia_next instanceof IdentIA) {
				IdentTableEntry ite = ((IdentIA) ia_next).getEntry();
				final String text = ite.getIdent().getText();
				if (text.equals("reflect")) {
					b = true;
					String text2 = String.format("ZS%d_reflect", code);
					addRef(text2, Ref.FUNCTION);
				} else if (text.equals("Type")) {
					b = true;
					String text2 = String.format("ZST%d", code); // return a TypeInfo structure
					addRef(text2, Ref.FUNCTION);
				} else if (text.equals("Name")) {
					b = true;
					String text2 = String.format("ZSN%d", code);
					addRef(text2, Ref.FUNCTION); // TODO make this not a function
				} else {
					assert i == sSize-1; // Make sure we are ending with a constructor call
					// README Assuming this is for named constructors
					String text2 = String.format("ZC%d%s", code, text);
					addRef(text2, Ref.CONSTRUCTOR);
				}
			} else {
				assert i == sSize-1; // Make sure we are ending with a constructor call
				String text2 = String.format("ZC%d", code);
				addRef(text2, Ref.CONSTRUCTOR);
			}
		} else if (resolved_element instanceof ConstructorDef) {
			assert i == sSize - 1; // Make sure we are ending with a constructor call
			int code;
			if (aResolved != null) {
				code = ((BaseGeneratedFunction) aResolved).getCode();
			} else {
				code = -1;
				System.err.println("** 31161 not resolved " + resolved_element);
			}
			// README Assuming this is for named constructors
			String text = ((ConstructorDef) resolved_element).name();
			String text2 = String.format("ZC%d%s", code, text);
			addRef(text2, Ref.CONSTRUCTOR);
		} else if (resolved_element instanceof FunctionDef) {
			OS_Element parent = resolved_element.getParent();
			int code = -1;
			if (aResolved != null) {
				if (aResolved instanceof BaseGeneratedFunction) {
					final BaseGeneratedFunction rf = (BaseGeneratedFunction) aResolved;
					GeneratedNode gc = rf.getGenClass();
					if (gc instanceof GeneratedContainerNC) // and not another function
						code = ((GeneratedContainerNC) gc).getCode();
					else
						code = -2;
				} else if (aResolved instanceof GeneratedClass) {
					final GeneratedClass generatedClass = (GeneratedClass) aResolved;
					code = generatedClass.getCode();
				}
			}
			// TODO what about overloaded functions
			assert i == sSize-1; // Make sure we are ending with a ProcedureCall
			sl.clear();
			if (code == -1) {
//				text2 = String.format("ZT%d_%d", enclosing_function._a.getCode(), closure_index);
			}
			String text2 = String.format("Z%d%s", code, ((FunctionDef) resolved_element).name());
			addRef(text2, Ref.FUNCTION);
		} else if (resolved_element instanceof DefFunctionDef) {
			OS_Element parent = resolved_element.getParent();
			int code;
			if (aResolved != null) {
				assert aResolved instanceof BaseGeneratedFunction;
				final BaseGeneratedFunction rf = (BaseGeneratedFunction) aResolved;
				GeneratedNode gc = rf.getGenClass();
				if (gc instanceof GeneratedContainerNC) // and not another function
					code = ((GeneratedContainerNC) gc).getCode();
				else
					code = -2;
			} else {
				if (parent instanceof ClassStatement) {
					code = ((ClassStatement) parent)._a.getCode();
				} else if (parent instanceof NamespaceStatement) {
					code = ((NamespaceStatement) parent)._a.getCode();
				} else {
					// TODO what about FunctionDef, etc
					code = -1;
				}
			}
			// TODO what about overloaded functions
			assert i == sSize-1; // Make sure we are ending with a ProcedureCall
			sl.clear();
			if (code == -1) {
//				text2 = String.format("ZT%d_%d", enclosing_function._a.getCode(), closure_index);
			}
			final DefFunctionDef defFunctionDef = (DefFunctionDef) resolved_element;
			String text2 = String.format("Z%d%s", code, defFunctionDef.name());
			addRef(text2, Ref.FUNCTION);
		} else if (resolved_element instanceof VariableStatement) {
			// first getParent is VariableSequence
			final String text2 = ((VariableStatement) resolved_element).getName();
			if (resolved_element.getParent().getParent() == generatedFunction.getFD().getParent()) {
				// A direct member value. Doesn't handle when indirect
//				text = Emit.emit("/*124*/")+"vsc->vm" + text2;
				addRef(text2, Ref.DIRECT_MEMBER, aValue);
			} else {
				final OS_Element parent = resolved_element.getParent().getParent();
				if (parent == generatedFunction.getFD()) {
					addRef(text2, Ref.LOCAL);
				} else {
//					if (parent instanceof NamespaceStatement) {
//						int y=2;
//					}
					addRef(text2, Ref.MEMBER, aValue);
				}
			}
		} else if (resolved_element instanceof PropertyStatement) {
			OS_Element parent = resolved_element.getParent();
			int code;
			if (parent instanceof ClassStatement) {
				code = ((ClassStatement) parent)._a.getCode();
			} else if (parent instanceof NamespaceStatement) {
				code = ((NamespaceStatement) parent)._a.getCode();
			} else {
//							code = -1;
				throw new IllegalStateException("PropertyStatement cant have other parent than ns or cls. " + resolved_element.getClass().getName());
			}
			sl.clear();  // don't we want all the text including from sl?
//			if (text.equals("")) text = "vsc";
//			text = String.format("ZP%dget_%s(%s)", code, ((PropertyStatement) resolved_element).name(), text); // TODO Don't know if get or set!
			String text2 = String.format("ZP%dget_%s", code, ((PropertyStatement) resolved_element).name()); // TODO Don't know if get or set!
			addRef(text2, Ref.PROPERTY_GET);
		} else if (resolved_element instanceof AliasStatement) {
			int y=2;
			NotImplementedException.raise();
//			text = Emit.emit("/*167*/")+((AliasStatement)resolved_element).name();
//			return _getIdentIAPath_IdentIAHelper(text, sl, i, sSize, _res)
		} else {
//						text = idte.getIdent().getText();
			System.out.println("1008 "+resolved_element.getClass().getName());
			throw new NotImplementedException();
		}
		return b;
	}

	@NotNull static List<InstructionArgument> _getIdentIAPathList(@NotNull InstructionArgument oo) {
		List<InstructionArgument> s = new LinkedList<InstructionArgument>();
		while (oo != null) {
			if (oo instanceof IntegerIA) {
				s.add(0, oo);
				oo = null;
			} else if (oo instanceof IdentIA) {
				final IdentTableEntry ite1 = ((IdentIA) oo).getEntry();
				s.add(0, oo);
				oo = ite1.getBacklink();
			} else if (oo instanceof ProcIA) {
//				final ProcTableEntry prte = ((ProcIA)oo).getEntry();
				s.add(0, oo);
				oo = null;
			} else
				throw new IllegalStateException("Invalid InstructionArgument");
		}
		return s;
	}

	private final static class BuildState {
		StringBuilder sb = new StringBuilder();
		boolean open = false, needs_comma = false;

		public void appendText(final String text, final boolean erase) {
			if (erase)
				sb = new StringBuilder();

			sb.append(text);
		}

		@Contract(pure = true)
		public @NotNull String asString() {
			return sb.toString();
		}

		@Contract(pure = true)
		@Override
		public @NotNull String toString() {
			return asString();
		}

		public @NotNull String closeOff(final List<String> args) {
			final boolean has_args = args != null && args.size() > 0;

			final StringBuilder sb1 = sb;

			//Preconditions.checkArgument(has_args && needs_comma); // want implies
			if (has_args)
				Preconditions.checkArgument(true || needs_comma);

			// short circuit
			if (args == null && open) {
				sb.append(")");
				// open = false; // yeah right
				return sb.toString();
			}

			if (needs_comma && has_args)
				sb.append(", ");

			if (open) {
				if (has_args) {
					sb1.append(Helpers.String_join(", ", args));
				}
				sb1.append(")");
			}

			return sb1.toString();
		}
	}

	@NotNull
	public String build() {
		final BuildState st = new BuildState();

		for (final Reference ref : refs) {
			switch (ref.type) {
			case LITERAL:
			case DIRECT_MEMBER:
			case INLINE_MEMBER:
			case MEMBER:
			case LOCAL:
			case FUNCTION:
			case PROPERTY_GET:
			case PROPERTY_SET:
			case CONSTRUCTOR:
				ref.buildHelper(st);
				break;
			default:
				throw new IllegalStateException("Unexpected value: " + ref.type);
			}
		}

		st.closeOff(args);

		//st.finish(this);

		return st.asString();
	}

	/**
	 * Call before you call build
	 *
	 * @param sl3
	 */
	public void args(List<String> sl3) {
		args = sl3;
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
