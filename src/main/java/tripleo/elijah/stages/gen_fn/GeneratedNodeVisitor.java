package tripleo.elijah.stages.gen_fn;

public interface GeneratedNodeVisitor {
	void visitFunction(GeneratedFunction aGeneratedFunction) throws Throwable;

	void visitConstructor(GeneratedConstructor aGeneratedConstructor);

	void visitNamespace(GeneratedNamespace aGeneratedNamespace) throws Throwable;

	void visitClass(GeneratedClass aGeneratedClass) throws Throwable;
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
