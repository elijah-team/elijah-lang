/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.gen_fn;

import com.google.common.base.Preconditions;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import tripleo.elijah.lang.BaseFunctionDef;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.FormalArgListItem;
import tripleo.elijah.lang.FunctionDef;
import tripleo.elijah.lang.OS_Element;
import tripleo.elijah.lang.OS_Module;
import tripleo.elijah.stages.instructions.VariableTableType;

import java.util.Collection;

/**
 * Created 6/27/21 9:40 AM
 */
public class GeneratedFunction extends BaseGeneratedFunction implements GNCoded {
	public final @Nullable FunctionDef fd;

	public GeneratedFunction(final @Nullable FunctionDef functionDef) {
		fd = functionDef;
	}

	//
	// region toString
	//

	@Override
	public String toString() {
		return asString();
	}

	public String asString() {
		Preconditions.checkNotNull(fd);

		final @Nullable Collection<FormalArgListItem> args = fd.getArgs();
		Preconditions.checkNotNull(args);

		final String pte_string = args.toString(); // TODO wanted PTE.getLoggingString

		final OS_Element parent = fd.getParent();
		final String     name   = fd.name();

		return asString(parent, name, pte_string);
	}

	private String asString(final @NotNull OS_Element aParent,
							final @NotNull String aName,
							final @NotNull String aPte_string) {
		final String s = String.format("<GeneratedFunction %s %s %s>", aParent, aName, aPte_string);
		return s;
	}

	public String name() {
		//if (fd == null)
		//	throw new IllegalArgumentException("null fd");
		Preconditions.checkNotNull(fd);

		return fd.name();
	}

	// endregion

	@Override
	public @NotNull BaseFunctionDef getFD() {
		//if (fd != null) return fd;
		//throw new IllegalStateException("No function");

		Preconditions.checkNotNull(fd);

		return fd;
	}

	@Override
	public VariableTableEntry getSelf() {
		final VariableTableEntry R;

		final OS_Element parent = getFD().getParent();

		if (parent instanceof ClassStatement) {
			R = getVarTableEntry(0);
			if (R.vtt != VariableTableType.SELF)
				throw new AssertionError("parent is class but vte(0) is not SELF");
		} else
			R = null;

		return R;
	}

	@Contract(pure = true)
	@Override
	public String identityString() {
		return String.format("%s", getFD());
	}

	@Contract(pure = true)
	@Override
	public Role getRole() {
		return Role.FUNCTION;
	}

	@Contract(pure = true)
	@Override
	public OS_Module module() {
        return getFD().getContext().module();
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
