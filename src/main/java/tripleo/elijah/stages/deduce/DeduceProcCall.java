/* -*- Mode: Java; tab-width: 4; indent-tabs-mode: t; c-basic-offset: 4 -*- */
/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.stages.deduce;

import tripleo.elijah.comp.ErrSink;
import tripleo.elijah.lang.BaseFunctionDef;
import tripleo.elijah.lang.ClassStatement;
import tripleo.elijah.lang.Context;
import tripleo.elijah.lang.FormalArgListItem;
import tripleo.elijah.lang.IdentExpression;
import tripleo.elijah.lang.LookupResultList;
import tripleo.elijah.lang.NormalTypeName;
import tripleo.elijah.lang.OS_Element;
import tripleo.elijah.lang.OS_Type;
import tripleo.elijah.lang.VariableStatement;
import tripleo.elijah.stages.gen_fn.BaseGeneratedFunction;
import tripleo.elijah.stages.gen_fn.IdentTableEntry;
import tripleo.elijah.stages.gen_fn.ProcTableEntry;
import tripleo.elijah.stages.gen_fn.VariableTableEntry;
import tripleo.elijah.stages.instructions.IdentIA;
import tripleo.elijah.stages.instructions.InstructionArgument;
import tripleo.elijah.stages.instructions.IntegerIA;
import tripleo.elijah.stages.instructions.ProcIA;
import tripleo.elijah.stages.instructions.VariableTableType;
import tripleo.elijah.util.Helpers;
import tripleo.elijah.util.NotImplementedException;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import com.google.common.base.Supplier;

import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * Created 11/30/21 11:56 PM
 */
public class DeduceProcCall {
	private final ProcTableEntry procTableEntry;
	private DeduceTypes2 deduceTypes2;
	private Context context;
	private BaseGeneratedFunction generatedFunction;
	private ErrSink errSink;

	@Contract(pure = true)
	public DeduceProcCall(final @NotNull ProcTableEntry aProcTableEntry) {
		procTableEntry = aProcTableEntry;
	}

	public void setDeduceTypes2(final DeduceTypes2 aDeduceTypes2,
								final Context aContext,
								final BaseGeneratedFunction aGeneratedFunction,
								final ErrSink aErrSink) {
		deduceTypes2      = aDeduceTypes2;
		context           = aContext;
		generatedFunction = aGeneratedFunction;
		errSink           = aErrSink;
	}

	DeduceElement target;

	public @Nullable DeduceElement target() {
		// short circuit
		if (target != null) return target;

		// dig in the bag
		final IdentIA         expression_num = (IdentIA) procTableEntry.expression_num;

		// local state
		final _Get_Target     gt             = new _Get_Target(errSink);

		try {
			final DeclTarget t = gt.act(expression_num, this);

			assert target == null || target == t;

			target = t;
		} catch (ResolveError aE) {
			errSink.reportDiagnostic(aE);
			throw new RuntimeException(aE);
		}

		return target;
	}

	private static class _Get_Target {
		private final ErrSink errSink;

		private _Get_Target(final ErrSink aErrSink) {
			errSink = aErrSink;
		}

		private DeclTarget act(final @NotNull IdentIA expression_num, final DeduceProcCall dpc) throws ResolveError {
			final @NotNull IdentTableEntry t = expression_num.getEntry();

			return act2(t, dpc);
		}

		private DeclTarget act2(final @NotNull IdentTableEntry t, final DeduceProcCall aDeduceProcCall) throws ResolveError {
			final DeclTarget          result;
			final InstructionArgument bl_ = t.getBacklink();

			if (bl_ == null) {
				result = act2_001_null_backlink(t, aDeduceProcCall);
			} else {
				result = act2_001_has_backlink(aDeduceProcCall, bl_);
			}

			return result;
		}

		private DeclTarget act2_001_has_backlink(final DeduceProcCall aDeduceProcCall, final InstructionArgument bl_) {
			final act2_001_has_backlink_IALogicProcessable processable =
					new act2_001_has_backlink_IALogicProcessable(aDeduceProcCall);
			DeduceTypes2.IALogic.process(bl_, processable);

			final DeclTarget result = processable.getTarget();
			return result;
		}

		@Contract("_, _ -> new")
		private @NotNull DeclTarget act2_001_null_backlink(final @NotNull IdentTableEntry t, final @NotNull DeduceProcCall dpc) throws ResolveError {
			final Context               context1           = dpc.context;
			final DeduceTypes2          deduceTypes21      = dpc.deduceTypes2;
			final BaseGeneratedFunction generatedFunction1 = dpc.generatedFunction;

			final LookupResultList     lrl  = DeduceLookupUtils.lookupExpression(t.getIdent(), context1, deduceTypes21);
			final @Nullable OS_Element best = lrl.chooseBest(null);
			assert best != null;

			final OS_Type attached = generatedFunction1.vte_list
					.stream()
					.filter(x -> x.vtt == VariableTableType.SELF)
					.collect(Collectors.toList())
					.get(0)
					.type.getAttached();
			assert attached != null;

			final ClassStatement self     = attached.getClassOf();
			final ClassStatement inherits = DeduceLocalVariable.class_inherits(self, best.getParent());

			final DeclAnchor.AnchorType anchorType;
			final OS_Element            declAnchor;

			if (inherits != null) {
				anchorType = DeclAnchor.AnchorType.INHERITED;
				declAnchor = inherits;
			} else {
				anchorType = DeclAnchor.AnchorType.MEMBER;
				declAnchor = self;
			}
			return new DeclTarget(best, declAnchor, anchorType, errSink, generatedFunction1, deduceTypes21);
		}

		private static class _Carrier {
			private final DeduceProcCall deduceProcCall;

			public _Carrier(final DeduceProcCall aDeduceProcCall) {
				deduceProcCall = aDeduceProcCall;
			}

			public void setTarget(final DeclTarget aTarget) {
				deduceProcCall.target = aTarget;
			}

			public BaseGeneratedFunction getGeneratedFunction() {
				return deduceProcCall.generatedFunction;
			}

			public DeduceTypes2 getDeduceTypes2() {
				return deduceProcCall.deduceTypes2;
			}

			@NotNull
			private DeclTarget getDeclTarget(final OS_Element resolved_element,
											 final DeclAnchor.AnchorType anchorType,
											 final OS_Element declAnchor) {
				final ErrSink errSink1 = deduceProcCall.errSink;

				final DeduceTypes2 dt2 = getDeduceTypes2();
				final BaseGeneratedFunction generatedFunction1 = getGeneratedFunction();

				return new DeclTarget(resolved_element, declAnchor, anchorType, errSink1, generatedFunction1, dt2);
			}
		}

		private static class act2_001_has_backlink_IALogicProcessable implements DeduceTypes2.IALogicProcessable {
			private final _Carrier carrier;
			private DeclTarget _target;

			public act2_001_has_backlink_IALogicProcessable(final DeduceProcCall aDeduceProcCall) {
				carrier = new _Carrier(aDeduceProcCall);
			}

			@Override
			public void doIntegerIA(final @NotNull IntegerIA aIntegerIA) {
				final @NotNull VariableTableEntry bl               = aIntegerIA.getEntry();
				final OS_Element                  resolved_element = bl.getResolvedElement();

				doIntegerIA_VTE(bl,
						resolved_element,
						carrier::getGeneratedFunction,
						x -> {_target = x;},
						carrier);
			}

			private static void doIntegerIA_VTE(final @NotNull VariableTableEntry ignoredVte,
												final OS_Element resolved_element,
												final @NotNull Supplier<BaseGeneratedFunction> f,
												final @NotNull Consumer<DeclTarget> ts,
												final @NotNull _Carrier cr) {
				final DeclAnchor.AnchorType anchorType;
				final OS_Element            declAnchor;

				// something (TODO)
				final BaseGeneratedFunction generatedFunction1 = f.get();

				// calculations
				if (resolved_element instanceof FormalArgListItem) {
					anchorType = DeclAnchor.AnchorType.PARAMS;
					declAnchor = generatedFunction1.getFD();
				} else if (resolved_element instanceof VariableStatement) {
					final OS_Element      parent = resolved_element.getParent().getParent();
					final BaseFunctionDef gf_fd  = generatedFunction1.getFD();

					if (parent == gf_fd) {
						anchorType = DeclAnchor.AnchorType.VAR;
						declAnchor = parent;
					} else
						throw new NotImplementedException();
				} else {
					anchorType = DeclAnchor.AnchorType.MEMBER;
					declAnchor = resolved_element instanceof IdentExpression ? resolved_element : resolved_element.getParent();
				}

				// effects
				final DeclTarget target1 = cr.getDeclTarget(resolved_element, anchorType, declAnchor);
				cr.setTarget(target1);

				ts.accept(target1);
				// _target = target1;
			}

			@Override public void doIdentIA(final IdentIA aIdentIA) {}

			@Override public void doProcIA(final ProcIA aProcIA) {
				final OS_Element resolved_element = aProcIA.getEntry().getResolvedElement();

				DeclAnchor.AnchorType anchorType = DeclAnchor.AnchorType.VAR; // TODO ??
				OS_Element            declAnchor = resolved_element/*.getParent()*/;

				final DeclTarget target1 = carrier.getDeclTarget(resolved_element, anchorType, declAnchor);
				carrier.setTarget(target1);

				_target = target1;
			}

			public DeclTarget getTarget() {
				return _target;
			}
		}
	}

	private static class DeclTarget implements DeduceElement {
		private @NotNull final OS_Element element;
		private @NotNull final DeclAnchor anchor;
		private @NotNull final DeduceTypes2 deduceTypes2;

		public DeclTarget(final @NotNull OS_Element aBest,
						  final @NotNull OS_Element aDeclAnchor,
						  final @NotNull DeclAnchor.AnchorType aAnchorType,
						  final @NotNull ErrSink errSink,
						  final @NotNull BaseGeneratedFunction aGeneratedFunction,
						  final @NotNull DeduceTypes2 aDeduceTypes2) {
			// given
			element      = aBest;
			deduceTypes2 = aDeduceTypes2;

			// created
			anchor       = new DeclAnchor(aDeclAnchor, aAnchorType);

			// action!
			final IInvocation invocation = getInvocation(aDeclAnchor, aAnchorType, errSink, aGeneratedFunction);
			anchor.setInvocation(invocation);
		}

		private IInvocation getInvocation(final @NotNull OS_Element aDeclAnchor,
										  final @NotNull DeclAnchor.AnchorType aAnchorType,
										  final @NotNull ErrSink errSink,
										  final @NotNull BaseGeneratedFunction aGeneratedFunction) {
			final IInvocation invocation;

			switch (aAnchorType) {
			case VAR:
				invocation = getInvocation_anchor_VAR(errSink);
				break;
			case MEMBER:
			case INHERITED:
			case FOREIGN:
			case CLOSURE:
			case PARAMS:
				invocation = getInvocation_anchor_other(aDeclAnchor, aAnchorType, aGeneratedFunction);
				break;
			default:
				throw new IllegalStateException("222 Unexpected value: " + aAnchorType);
			}

			return invocation;
		}

		private IInvocation getInvocation_anchor_other(final @NotNull OS_Element 			aDeclAnchor,
													   final @NotNull DeclAnchor.AnchorType aAnchorType,
													   final @NotNull BaseGeneratedFunction aGeneratedFunction) {
			final IInvocation invocation;
    /*
					final IInvocation declaredInvocation = aGeneratedFunction.fi.getClassInvocation();

					if (declaredInvocation == null) {
						declaredInvocation = aGeneratedFunction.fi.getNamespaceInvocation();
					}
	*/

			final IInvocation declaredInvocation = Helpers.sequential_choose(
					aGeneratedFunction.fi,
					FunctionInvocation::getClassInvocation,
					FunctionInvocation::getNamespaceInvocation);

			if (aAnchorType == DeclAnchor.AnchorType.INHERITED) {
				assert declaredInvocation instanceof ClassInvocation;

				//assert ((ClassInvocation) declaredInvocation).getKlass() == aDeclAnchor;

				invocation = new DerivedClassInvocation((ClassStatement) aDeclAnchor, (ClassInvocation) declaredInvocation);
			} else {
				invocation = declaredInvocation;
			}
			return invocation;
		}

		@NotNull
		private IInvocation getInvocation_anchor_VAR(final @NotNull ErrSink errSink) {
			final IInvocation       invocation;
			if (element instanceof VariableStatement) {
				final VariableStatement variableStatement = (VariableStatement) element;
				final NormalTypeName    normalTypeName    = (NormalTypeName) variableStatement.typeName();

				final Context           normalTypeNameContext = normalTypeName.getContext();
				final String            normalTypeNameName    = normalTypeName.getName();

				final LookupResultList  lrl                   = normalTypeNameContext.lookup(normalTypeNameName);
				final ClassStatement    classStatement        = (ClassStatement) lrl.chooseBest(null);

				invocation = DeduceTypes2.ClassInvocationMake.withGenericPart(classStatement, null, normalTypeName, deduceTypes2, errSink);
			} else if (element instanceof ClassStatement) {
				final ClassStatement classStatement = (ClassStatement) element;
				final NormalTypeName normalTypeName           = null;

				invocation = DeduceTypes2.ClassInvocationMake.withGenericPart(classStatement, null, normalTypeName, deduceTypes2, errSink);
			} else
				throw new IllegalStateException("365!! Can't create invocation");
			return invocation;
		}

		@Override
		public OS_Element element() {
			return element;
		}

		@Override
		public DeclAnchor declAnchor() {
			return anchor;
		}
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
