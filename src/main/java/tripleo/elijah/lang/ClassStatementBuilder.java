package tripleo.elijah.lang;

import static tripleo.elijah.util.Helpers.List_of;

public class ClassStatementBuilder {
	private OS_Element parentElement;
	private Context parentContext;
	private IdentExpression nameToken;

	public ClassStatementBuilder setParentElement(final OS_Element aParentElement) {
		parentElement = aParentElement;
		return this;
	}

	public ClassStatementBuilder setParentContext(final Context aParentContext) {
		parentContext = aParentContext;
		return this;
	}

	public ClassStatement createClassStatement() {
		final ClassStatement classStatement = new ClassStatement(parentElement, parentContext);

		// TODO access to ctor params
		final ClassHeader ch = new ClassHeader(false, List_of());
		if (nameToken != null) {
			ch.setName(nameToken);
		}
		classStatement.setHeader(ch);

		return classStatement;
	}

	public ClassStatementBuilder setName(final IdentExpression aNameToken) {
		nameToken = aNameToken;
		return this;
	}
}
