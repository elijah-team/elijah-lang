/*
 * Elijjah compiler, copyright Tripleo <oluoluolu+elijah@gmail.com>
 *
 * The contents of this library are released under the LGPL licence v3,
 * the GNU Lesser General Public License text was downloaded from
 * http://www.gnu.org/licenses/lgpl.html from `Version 3, 29 June 2007'
 *
 */
package tripleo.elijah.entrypoints;

/**
 * Created 6/14/21 7:20 AM
 */
public interface EntryPoint {
	interface EntryPointVisitor {
		void visitMainClass(MainClassEntryPoint mcep);
		void visitArbitrary(ArbitraryFunctionEntryPoint afep);
	}

	final class EntryPointVisit {
		public static void visit(EntryPoint ep, EntryPointVisitor epv) {
			if (ep instanceof MainClassEntryPoint) {
				epv.visitMainClass((MainClassEntryPoint) ep);
				return;
			}

			if (ep instanceof ArbitraryFunctionEntryPoint) {
				epv.visitArbitrary((ArbitraryFunctionEntryPoint) ep);
				return;
			}

			throw new IllegalStateException("Can't cast EntryPoint");
		}
	}
}

//
// vim:set shiftwidth=4 softtabstop=0 noexpandtab:
//
